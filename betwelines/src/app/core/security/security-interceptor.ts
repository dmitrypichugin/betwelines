import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable, of } from "rxjs";
import { TokenStorageService } from "./token-storage.service";

const TOKEN_HEADER_KEY = 'Authorization';


@Injectable()
export class SecurityInterceptor implements HttpInterceptor {

	constructor(private token: TokenStorageService, private router: Router) { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		let authReq = req;
		if (!(req.url.indexOf("oauth/token") > 0 /*&& req.method === "POST"*/)) {
			if (this.token.getToken() != null) {
				// console.log("INTERCEPTOR: token exists, adding it to request: ", this.token.getToken()["access_token"]);
				authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + this.token.getToken()["access_token"]) });
			} else {
				// console.log("INTERCEPTOR: auth url: ", req.url);
				this.router.navigate(['login']);
				return of(<HttpEvent<any>>{});
			}
		}
		return next.handle(authReq);/*.pipe(
			catchError(error => {
			  if (error.status === 401 || error.status === 403) {
				// handle error
			  }
			  return throwError(error);
			})
		 )*/
	}

}
