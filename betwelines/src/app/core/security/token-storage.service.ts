import { Injectable } from '@angular/core';
import { User } from './user/user';

const TOKEN_KEY = 'token';

@Injectable({
	providedIn: 'root'
})
export class TokenStorageService {

	constructor() { }

	signOut() {
		// window.sessionStorage.removeItem(TOKEN_KEY);
		// window.sessionStorage.clear();
		localStorage.removeItem(TOKEN_KEY);
	}

	saveToken(token: string | Object) {
		// window.sessionStorage.removeItem(TOKEN_KEY);
		// window.sessionStorage.setItem(TOKEN_KEY, token);
		localStorage.setItem('token', typeof(token) === 'string' ? token : JSON.stringify(token));
	}

	getToken(): string {
		// return window.sessionStorage.getItem(TOKEN_KEY);
		return JSON.parse(localStorage.getItem(TOKEN_KEY));
	}

}
