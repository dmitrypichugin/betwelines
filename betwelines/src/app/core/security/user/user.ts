export interface User {
	id: string;
	password: string;

	// Token related fields:
	username: string;
	token?: string;

	// Model related fields (User object on the back-end):
	name: string;
	email: string;
	roles: string[];
}
