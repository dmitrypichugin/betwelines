import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

	constructor(
		private router: Router,
		private authService: AuthService
	) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		const url = state.url;
		// console.log("1: " + url);
		return this.authService.getCurrentUser().pipe(
			map(user => {
				// console.log("2: ", url, "; user: ", user);
				if (user) {
					return true;
				}
				// console.log("3: " + url);
				this.router.navigate(['/login'], { queryParams: { returnUrl: url } });
				return false;
			}),
			catchError((err) => {
				// console.log("3: " + url, err);
				this.router.navigate(['/login'], { queryParams: { returnUrl: url } });
				// return false;
				return of(false);
			}));
	}

}