import { Injectable } from '@angular/core';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { map, flatMap, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { User } from './user/user';
import { TokenStorageService } from './token-storage.service';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	private currentUserSubject: Subject<User> = new ReplaySubject(1/*JSON.parse(localStorage.getItem('currentUser'))*/);
	private currentUser: Observable<User>;


	constructor(
		private http: HttpClient,
		private tokenStorage: TokenStorageService) {}

	getCurrentUser(): Observable<User> {
		if (!this.currentUser) {
			this.currentUser = this.currentUserSubject.asObservable(); //.pipe(flatMap(() => this.loadUser()));
			this.loadUser().subscribe();
		}
		return this.currentUser;
	}

	private loadUser(): Observable<User> {
		let token = this.tokenStorage.getToken();
		if (token && token["access_token"]) {
			return this.http.get<User>(`${environment.apiUrl}/api/users/current`, {
				headers: { "Authorization": "Bearer " + token["access_token"] }
			}).pipe(
				map(user => {
					// console.log("Retrieved current user: ", user);
					this.currentUserSubject.next(user);
					return user;
				})
				,
				catchError(err => {
					// console.log("Retrieved null user", err);
					this.currentUserSubject.next(null);
					return of(null);
				}
				));
		}
		// console.log("loadUser() returns null, token was: ", token);
		this.currentUserSubject.next(null);
		return of(null);
	}

	login(username: string, password: string): Observable<User> {
		return this.http.post<any>(`${environment.apiUrl}/oauth/token`, `username=${username}&password=${password}&grant_type=password`,
			{
				headers: {
					"Content-Type": "application/x-www-form-urlencoded",
					"Authorization": "Basic c3lucXVhcml1bS13ZWItY2xpZW50OnN5bnF1YXJpdW0td2ViLWNsaWVudC1zZWNyZXQ="
				}
			}).pipe(flatMap(token => {
				// login successful if there's a jwt token in the response
				if (token && token["access_token"]) {
					// store user details and jwt token in local storage to keep user logged in between page refreshes
					// localStorage.setItem('token', JSON.stringify(token));
					// console.log("TOKEN: ", token);
					this.tokenStorage.saveToken(token)
					// return this.http.get<User>(`${environment.apiUrl}/api/users/current`, {
					// 	headers: { "Authorization": "Bearer " + token["access_token"] }
					// }).pipe(map(user => {
					// 	console.log("Retrieved current user: ", user);
					// 	this.currentUserSubject.next(user);
					// 	return user;
					// }));
				}
				// return of(<User>{});
				return this.loadUser();
			}));
	}

	logout() {
		// remove user from local storage to log user out
		localStorage.removeItem('token');
		this.currentUserSubject.next(null);
	}

}
