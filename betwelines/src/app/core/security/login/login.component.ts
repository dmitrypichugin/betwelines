import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
	selector: 'betwelines-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	error: string;
	loading: boolean;
	username: string;
	password: string;
	returnUrl: string;

	constructor(
		private authService: AuthService,
		private route: ActivatedRoute,
		private router: Router) { }

	ngOnInit() {
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
	}

	login() {
		this.loading = true;
		this.authService.login(this.username, this.password).subscribe(user => {
			console.log("Login successful, redirecting to: ", this.returnUrl, user);
			this.router.navigate([this.returnUrl]);
		},
		error => {
			this.error = error;
			this.loading = false;
		});
	}

}
