import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardsComponent } from './components/cards/cards.component';
import { SettingsComponent } from './components/settings/settings.component';
import { LoginComponent } from './core/security/login/login.component';
import { AuthGuard } from './core/security/auth-guard';
import { MainComponent } from './components/main/main.component';
import { ArticleComponent } from './components/article/article.component';

const routes: Routes = [
	{ path: 'docs/:version', component: MainComponent, canActivate: [AuthGuard],
		children: [
			{ path: ':articleHandle', component: ArticleComponent, canActivate: [AuthGuard]}
		] },
	{ path: 'cards', component: CardsComponent, canActivate: [AuthGuard] },
	{ path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },
	{ path: 'login', component: LoginComponent },
	{ path: '', component: MainComponent, canActivate: [AuthGuard] },
	// { path: '', redirectTo: '/docs', pathMatch: 'full' },
	{ path: '**', redirectTo: '' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
