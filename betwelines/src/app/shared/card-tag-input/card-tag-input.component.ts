import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, OnInit, Input } from '@angular/core';
import { CardTag } from '../card-tag/card-tag';
import { MatChipInputEvent } from '@angular/material/chips';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
	selector: 'betwelines-card-tag-input',
	templateUrl: './card-tag-input.component.html',
	styleUrls: ['./card-tag-input.component.css'],
	providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: CardTagInputComponent,
		multi: true
	}]
})
export class CardTagInputComponent implements ControlValueAccessor {
	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	disabled: boolean;
	readonly separatorKeysCodes: number[] = [ENTER, COMMA];

	onChangeHandler: (_: any) => void;
	onTouchHandler: () => void;
	tags: Array<CardTag>;
	
	writeValue(tags: Array<CardTag>): void {
		this.tags = tags;
	}
	registerOnChange(fn: any): void {
		this.onChangeHandler = fn;
	}
	registerOnTouched(fn: any): void {
		this.onTouchHandler = fn;
	}
	setDisabledState?(isDisabled: boolean): void {
		this.disabled = isDisabled;
	}

	add(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value;

		if ((value || '').trim()) {
			this.tags = this.tags || [];
			this.tags.push({ name: value.trim() });
		}

		// Reset the input value
		if (input) {
			input.value = '';
		}

		this.onChangeHandler(this.tags);
	}

	remove(tag: CardTag): void {
		const index = this.tags.indexOf(tag);
		if (index >= 0) {
			this.tags.splice(index, 1);
		}
		this.onChangeHandler(this.tags);
	}

}
