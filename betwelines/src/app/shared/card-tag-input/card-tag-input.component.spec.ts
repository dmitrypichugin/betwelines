import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTagInputComponent } from './card-tag-input.component';

describe('CardTagInputComponent', () => {
  let component: CardTagInputComponent;
  let fixture: ComponentFixture<CardTagInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTagInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTagInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
