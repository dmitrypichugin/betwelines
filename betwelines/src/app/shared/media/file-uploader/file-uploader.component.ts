import { Component, OnInit, Inject } from '@angular/core';
import { MediaService } from '../media.service';
import { map, catchError } from 'rxjs/operators';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs/internal/observable/of';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';

@Component({
	selector: 'betwelines-file-uploader',
	templateUrl: './file-uploader.component.html',
	styleUrls: ['./file-uploader.component.css']
})
export class FileUploaderComponent implements OnInit {

	files  = [];  

	constructor(
		private mediaService: MediaService,
		@Inject(MAT_SNACK_BAR_DATA) public data: any,
		public snackBarRef: MatSnackBarRef<FileUploaderComponent>
	) {
		console.log(data);
		// this.files.push(data.files);
		this.files = data.files;
		this.uploadFiles();
	}

	ngOnInit(): void {
	}

	closeMe() {
		this.files = [];
		this.snackBarRef.dismiss();
	}

	uploadImage(file): void {
		const formData = new FormData();
		formData.append('file', file.data);
		file.inProgress = true;
		this.mediaService.uploadFile(formData).pipe(
			map(event => {
				// console.log("Progress tracking: ", event);
				switch (event.type) {
					case HttpEventType.UploadProgress:
						file.progress = Math.round(event.loaded * 100 / event.total);
						break;
					case HttpEventType.Response:
						file.inProgress = false;
						this.data.onFileUploaded(event.body);
						return event;
				}
			}),
			catchError((error: HttpErrorResponse) => {
				console.log("CAUGHT ERROR: ", error);
				file.inProgress = false;
				return of(`${file.data.name} upload failed.`);
			})
		).subscribe((event: any) => {
			if (typeof (event) === 'object') {
				console.log(event);
			}
		});
	}

	private uploadFiles() {
		// this.fileUpload.nativeElement.value = '';
		this.files.forEach(file => {
			this.uploadImage(file);
		});
	}

}
