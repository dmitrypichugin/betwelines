export interface MediaItem { //TODO: rename to MediaDescriptor to be consistent with the back-end naming
	id: string;
	fileName: string;
	url: string;
	title: string;


	//From MediaBlob
	blobId?: string;
	mediaType?: string; // 'IMAGE' or 'VIDEO'
	// fileName: string;
	description?: string;
	format?: string;

	//From MediaDescriptor
	version?: string;
	articleId?: string;
	cardId?: string; //TODO: deprecated - most likely we won't do media for cards.
}
