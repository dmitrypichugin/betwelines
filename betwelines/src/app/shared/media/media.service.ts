import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { MediaItem } from './media-item';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class MediaService {

	constructor(private http: HttpClient) { }

	getListOfFiles(/*version: String, folderId: String */): Observable</*MediaItem[]*/string[]> {
		// return this.http.get<MediaItem[]>(`${environment.apiUrl}/api/betwelines/${version}/media-folders/${folderId}`);
		return this.http.get<string[]>(`${environment.apiUrl}/api/betwelines/media/images`);
	}

	uploadFile(/*version: any, articleId: any,*/ file: any): Observable</*MediaItem*/ HttpEvent<string>> {
		// return this.http.post<MediaItem>(`${environment.apiUrl}/api/betwelines/${version}/articles/${articleId}/media`, file);
		return this.http.post<string>(`${environment.apiUrl}/api/betwelines/media/images`, file, {
			reportProgress: true,
			observe: 'events'
		});
	}

	deleteFile(id: string): Observable<void> {
		return this.http.delete<void>(`${environment.apiUrl}/api/betwelines/media/images/${id}`);
	}

}
