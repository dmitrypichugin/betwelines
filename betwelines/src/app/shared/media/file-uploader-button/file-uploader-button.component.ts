import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FileUploaderComponent } from '../file-uploader/file-uploader.component';

@Component({
	selector: 'betwelines-file-uploader-button',
	templateUrl: './file-uploader-button.component.html',
	styleUrls: ['./file-uploader-button.component.css']
})
export class FileUploaderButtonComponent implements OnInit {

	@ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;
	@Output() onFileUploaded: EventEmitter<any> = new EventEmitter();

	files: any[] = [];

	constructor(public snackBar: MatSnackBar) { }

	ngOnInit(): void {
	}

	onClick() {
		const fileUpload = this.fileUpload.nativeElement;
		fileUpload.onchange = () => {
			for (let index = 0; index < fileUpload.files.length; index++) {
				const file = fileUpload.files[index];
				this.files.push({ data: file, inProgress: false, progress: 0 });
			}
			this.openSnackBar(this.files);
		};
		fileUpload.click();
	}

	openSnackBar(files: any[]) {
		this.snackBar.openFromComponent(FileUploaderComponent, {
			duration: 86400000, // 86400000 is 24 hours in milliseconds - we don't want to close the snackbar automatically
			horizontalPosition: 'right',
			verticalPosition: 'bottom',
			data: { files: files, onFileUploaded: (mediaBlob) => this.onFileUploaded.emit(mediaBlob) }
		});
	}

}
