import { Component, OnInit, Input } from '@angular/core';
import { CardTag } from './card-tag';

@Component({
	selector: 'betwelines-card-tag',
	templateUrl: './card-tag.component.html',
	styleUrls: ['./card-tag.component.css']
})
export class CardTagComponent implements OnInit {

	@Input() tags: Array<CardTag>;

	constructor() { }

	ngOnInit() {
	}

}
