import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface ConfirmationDialogData {
	title: string;
	body: string;
	cancelBtn: boolean;
}

export declare const enum ConfirmationDialogResult {
	OK = 1,
	CANCEL = 2
}

@Component({
	selector: 'betwelines-confirmation-dialog',
	templateUrl: './confirmation-dialog.component.html',
	styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent {

	constructor(
		public dialogRef: MatDialogRef<ConfirmationDialogComponent, ConfirmationDialogResult>,
		@Inject(MAT_DIALOG_DATA) public data: ConfirmationDialogData) {}

	ok(): void {
		this.dialogRef.close(ConfirmationDialogResult.OK);
	}

	cancel(): void {
		this.dialogRef.close(ConfirmationDialogResult.CANCEL);
	}

}
