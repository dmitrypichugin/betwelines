import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatChipsModule } from '@angular/material/chips';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTreeModule } from '@angular/material/tree';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTabsModule } from '@angular/material/tabs';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AppRoutingModule } from '../app-routing.module';
import { CardTagComponent } from './card-tag/card-tag.component';
import { CardTagInputComponent } from './card-tag-input/card-tag-input.component';
import { MarkdownModule } from 'ngx-markdown';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { FileUploaderComponent } from './media/file-uploader/file-uploader.component';
import { FileUploaderButtonComponent } from './media/file-uploader-button/file-uploader-button.component';
import { ClipboardModule } from '@angular/cdk/clipboard';

@NgModule({
	declarations: [CardTagComponent, CardTagInputComponent, ConfirmationDialogComponent, FileUploaderComponent, FileUploaderButtonComponent],
	imports: [
		CommonModule,
		FormsModule,
		HttpClientModule,
		ClipboardModule,
		MatCardModule,
		MatInputModule,
		MatIconModule,
		MatButtonModule,
		MatToolbarModule,
		MatDialogModule,
		MatSelectModule,
		MatDividerModule,
		MatListModule,
		MatMenuModule,
		MatChipsModule,
		MatSidenavModule,
		MatTreeModule,
		MatExpansionModule,
		MatTabsModule,
		MatProgressBarModule,
		MatSnackBarModule,
		MarkdownModule.forChild(),
		AppRoutingModule,
	],
	exports: [
		FormsModule,
		HttpClientModule,
		ClipboardModule,
		AppRoutingModule,
		CardTagComponent,
		CardTagInputComponent,
		MarkdownModule,
		MatCardModule,
		MatChipsModule,
		MatButtonModule,
		MatDialogModule,
		MatDividerModule,
		MatExpansionModule,
		MatInputModule,
		MatIconModule,
		MatListModule,
		MatMenuModule,
		MatSelectModule,
		MatSidenavModule,
		MatTabsModule,
		MatToolbarModule,
		MatTreeModule,
		MatProgressBarModule,
		MatSnackBarModule,
		FileUploaderComponent,
		FileUploaderButtonComponent,
	],
})
export class SharedModule { }
