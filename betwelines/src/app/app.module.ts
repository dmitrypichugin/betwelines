import { NgModule, SecurityContext } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentsModule } from './components/components.module';
import { SecurityInterceptor } from './core/security/security-interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { MarkdownModule } from 'ngx-markdown';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		CoreModule,
		ComponentsModule,
		BrowserAnimationsModule, //TODO: try to move this to the shared module
		MarkdownModule.forRoot({
			sanitize: SecurityContext.NONE // This is to allow for embedded videos in Markdown
		}),
	],
	providers: [
		{ provide: HTTP_INTERCEPTORS, useClass: SecurityInterceptor, multi: true },
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
