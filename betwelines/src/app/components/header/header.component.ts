import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/core/security/auth.service';
import { Router } from '@angular/router';
import { User } from 'src/app/core/security/user/user';
import { Subscription } from 'rxjs';

@Component({
	selector: 'betwelines-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

	currentUser: User;
	userSubscription: Subscription;

	constructor(
		private router: Router,
		private authService: AuthService) { }

	ngOnInit() {
		this.userSubscription = this.authService.getCurrentUser().subscribe(user => this.currentUser = user);
	}

	ngOnDestroy() {
		this.userSubscription.unsubscribe();
	}

	logout() {
		this.authService.logout();
		this.router.navigate(['/login']);
	}

}
