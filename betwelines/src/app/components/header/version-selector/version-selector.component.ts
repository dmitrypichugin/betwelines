import { Component, OnInit, OnDestroy } from '@angular/core';
import { VersionsService } from '../../versions/versions.service';
import { VersionsStoreService } from '../../versions/versions-store.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
	selector: 'betwelines-version-selector',
	templateUrl: './version-selector.component.html',
	styleUrls: ['./version-selector.component.css']
})
export class VersionSelectorComponent implements OnInit, OnDestroy {
	
	versions: string[]; //Version[];
	selectedVersion: string; //Version;
	subscriptions: Subscription = new Subscription();

	constructor(
		private versionService: VersionsService,
		private versionsStoreService: VersionsStoreService,
		private router: Router) { }

	ngOnInit(): void {
		this.versionService.getVersions().subscribe(versions => {
			if (versions && versions.length > 0) {
				this.versions =  versions.map(v => v.label);
				this.versionsStoreService.setDefaultVersion(this.versions[this.versions.length - 1]);
			}
		});
		this.subscriptions.add(this.versionsStoreService.getSelectedVersion().subscribe(
			v => this.selectedVersion = v));
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	onVersionSelected(version: string) {
		this.router.navigate(['/docs', version /*{version: version}*/]);
		// this.versionsStoreService.setSelectedVersion(version);
	}

}
