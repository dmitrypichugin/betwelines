import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Card } from '../../cards/card';
import { CardEditorComponent } from '../../cards/card-editor/card-editor.component';
import { CardViewerComponent } from '../../cards/card-viewer/card-viewer.component';
import { CardsService } from '../../cards/cards.service';
import { VersionsStoreService } from '../../versions/versions-store.service';
import { Article } from '../article';
import { ArticleStoreService } from '../article-store.service';

@Component({
	selector: 'betwelines-card-list',
	templateUrl: './card-list.component.html',
	styleUrls: ['./card-list.component.css']
})
export class CardListComponent implements OnInit, OnDestroy {

	cards: Card[];

	version: string;

	selectedArticle: Article;

	subscription: Subscription = new Subscription();

	constructor(
		private articleStoreService: ArticleStoreService,
		private cardsService: CardsService,
		private versionsStoreService: VersionsStoreService,
		private dialog: MatDialog,
		private cardService: CardsService,
		) { }

	ngOnInit(): void {
		this.subscription.add(this.versionsStoreService.getSelectedVersion().subscribe(version => {
			this.version = version;
			this.subscription.add(this.articleStoreService.getSelectedArticle().subscribe(article => {
				this.selectedArticle = article;
				if (article) {
					this.cardsService.getCardsByArticle(version, article.handle).subscribe(cards => this.cards = cards);
				} else {
					this.cards = [];
				}
			}));
		}));
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}

	add() {
		const dialogRef = this.dialog.open(CardEditorComponent, {
			width: '600px',
			data: { parentHandle: this.selectedArticle.handle }
		});
		dialogRef.beforeClosed().subscribe(result => {
			// result.tags = [... result.tags.split(",").map(tag => tag.trim())];
			result && this.cardService.createCard(this.version, result).subscribe(card => this.cards = [card, ...this.cards]);
		});
	}

	edit(card: Card) {
		const dialogRef = this.dialog.open(CardEditorComponent, {
			width: '600px',
			data: Object.assign({}, card)
		});
		dialogRef.beforeClosed().subscribe(result => {
			result && this.cardService.updateCard(this.version, result).subscribe(card => console.log("TODO: Update the search results..."));
		});
	}

	view(card: Card) {
		this.dialog.open(CardViewerComponent, {
			width: '660px',
			data: {version: this.version, cardName: card.handle}
		});
	}

	delete(card: Card) {
		this.cardService.deleteCard(this.version, card.id).subscribe(card => console.log("TODO: Update the search results..."));
	}
}
