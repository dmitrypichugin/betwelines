import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from './article';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ArticleService {

	constructor(private http: HttpClient,
		) { }

	public getArticleByHandle(version: string, articleHandle: string): Observable<Article> {
		return this.http.get<Article>(`${environment.apiUrl}/api/betwelines/${version}/articles?handle=${articleHandle}`);
	}

	public getArticle(version: string, articleId: string): Observable<Article> {
		return this.http.get<Article>(`${environment.apiUrl}/api/betwelines/${version}/articles/${articleId}`);
	}

	public updateArticle(version: string, article: Article): Observable<Article> {
		return this.http.put<Article>(`${environment.apiUrl}/api/betwelines/${version}/articles/${article.id}`, article);
	}

	public createArticle(version: string, article: Article): Observable<Article> {
		return this.http.post<Article>(`${environment.apiUrl}/api/betwelines/${version}/articles`, article);
	}

	public deleteArticle(version: string, articleId: string): Observable<void> {
		return this.http.delete<void>(`${environment.apiUrl}/api/betwelines/${version}/articles/${articleId}`);
	}

	public findArticles(version: string, searchText: string, page: number = 0, pageSize: number = 10): Observable<Array<Article>> {
		return this.http.get<Array<Article>>(
			`${environment.apiUrl}/api/betwelines/${version}/articles?searchText=${searchText}&from=${page * pageSize}&pageSize=${pageSize}`);
	}

}
