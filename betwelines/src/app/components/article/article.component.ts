import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Article } from './article';
import { ArticleStoreService } from './article-store.service';
import { ArticleService } from './article.service';
import { VersionsStoreService } from '../versions/versions-store.service';
import { ConfirmationDialogComponent, ConfirmationDialogResult } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { TextEditorService } from '../text-editor/text-editor.service';
import { TextEditorComponent } from '../text-editor/text-editor.component';

@Component({
	selector: 'betwelines-article',
	templateUrl: './article.component.html',
	styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit, OnDestroy {

	textEditor: TextEditorComponent;
	@ViewChild("textEditor") set content(textEditor: TextEditorComponent) {
		if (textEditor) {
			this.textEditor = textEditor;
			textEditor.setMarkdown(this.editedArticle.content);
		}
	}

	version: string;
	article: Article;
	subscription: Subscription = new Subscription();
	
	editMode: boolean;
	editedArticle: Article;

	editorOptions = {
		// initialValue: `# Title of Project` ,
		editorId: "mainArticleEditor", // An arbitrary name, used by the toastui internally.
		initialEditType: "wysiwyg", //"markdown",
		previewStyle: 'vertical',
		height: 'auto',
		minHeight: '500px'
	};
	viewerOptions = {
		editorId: "mainArticleViewer", // An arbitrary name, used by the toastui internally.
		viewer: true
		// initialEditType: "wysiwyg", //"markdown",
		// previewStyle: 'vertical',
		// height: 'auto',
		// minHeight: '500px'
	};

	resourcePanelOpen: boolean;

	constructor(
		private route: ActivatedRoute,
		private articleService: ArticleService,
		private articleStoreService: ArticleStoreService,
		private versionsStoreService: VersionsStoreService,
		private router: Router,
		private confirmationDialog: MatDialog,
		private editorService: TextEditorService,
	) { }

	ngOnInit() {
		this.subscription.add(this.route.paramMap.subscribe(params => {
			const articleHandle = params.get('articleHandle');
			this.subscription.add(this.versionsStoreService.getSelectedVersion().subscribe(version => {
				this.version = version;
				articleHandle && this.articleService.getArticleByHandle(version, articleHandle).subscribe(
					article => {
						this.article = article;
						this.articleStoreService.setSelectedArticle(article);
						this.editorService.setMarkdown(article.content, "mainArticleEditor");
					});
			}));
		}));
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}

	editArticle(article: Article): void {
		this.editMode = true;
		this.editedArticle = <Article> { ...article };
		// this.editorService.setMarkdown(article.content, "mainArticleEditor");
		// this.textEditor.setMarkdown(article.content);
	}

	cancelEdit(): void {
		this.editMode = false;
		this.editedArticle = null;
	}

	setChangedArticleContent(markdown: string): void {
		this.editedArticle.content = markdown;
	}

	saveArticle(): void {
		this.articleService.updateArticle(this.version, this.editedArticle).subscribe(article => {
			this.article = article; //<Article> { ...this.editedArticle }; //TODO: this is a temporary thing until we implement save...
			this.editMode = false;
			this.editedArticle = null;
		});
	}

	deleteArticle(): void {
		if (this.version && this.article.id) {
			let dialogRef = this.confirmationDialog.open(ConfirmationDialogComponent, {
				height: '200px',
				width: '500px',
				data: { 
					title: "Delete Article",
					body: "Are you sure you would like to delete the article '" + this.article.tocLabel + "'?",
					cancelBtn: true
				}
			});
			dialogRef.afterClosed().subscribe(result => {
				console.log(`Dialog result: ${result}`); // Pizza!
				if (result === ConfirmationDialogResult.OK) {
					this.articleService.deleteArticle(this.version, this.article.id).subscribe(() => 
					this.router.navigate(['/']));
				}
			});
		}
	}

}
