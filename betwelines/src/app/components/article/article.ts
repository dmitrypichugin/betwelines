import { Card } from "../cards/card";

export interface Article {
	id: string;
	title: string;
	content: string;
	/** A hidden note that is displayed to the users that have permissions. */
	note: string;
	cards: Card[];

	parentHandle: string; // This is only used when we create a new article as a child, 
	                // we specify parent's id and submit the object to the server. This is the only use case. 
	
	handle: string;  // : "my-mega-top-article", //what appears in the URL:
							// /betwelines/V.1.2.3/my-mega-top-article/my-sub-article
	tocLabel: string; //: "My Mega Top Article",      //what appears in the TOC menu and in the breadcrumbs
	order: number;  // : 9, // might be used in the future to sort articles within the parent
	created: string; //Date;   // : "2020-02-21T10:14:00.000Z", // ISO format (new Date().toISOString() in JavaScript)
	updated: Date;   // : "2020-02-21T10:14:00.000Z"

}