import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Clipboard } from '@angular/cdk/clipboard';
import { MediaService } from 'src/app/shared/media/media.service';
import { ArticleStoreService } from '../article-store.service';
import { Subscription } from 'rxjs';
import { VersionsStoreService } from '../../versions/versions-store.service';
import { MediaItem } from 'src/app/shared/media/media-item';
import { environment } from 'src/environments/environment';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogResult } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';

@Component({
	selector: 'betwelines-images-panel',
	templateUrl: './images-panel.component.html',
	styleUrls: ['./images-panel.component.css']
})
export class ImagesPanelComponent implements OnInit, OnDestroy {

	subscriptions = new Subscription();
	fileItems: MediaItem[];
	version: string;
	articleId: string;

	constructor(
		private mediaService: MediaService,
		private articleStoreService: ArticleStoreService,
		private versionStoreService: VersionsStoreService,
		private clipboard: Clipboard,
		private dialog: MatDialog) { }

	ngOnInit(): void {
		this.subscriptions.add(this.articleStoreService.getSelectedArticle().subscribe(article => {
			this.subscriptions.add(this.versionStoreService.getSelectedVersion().subscribe(version => {
				this.version = version;
				this.articleId = article.id;
				this.mediaService.getListOfFiles(/*version, article.id*/).subscribe(
					ids => this.fileItems = ids.map(id => {
						return {id: id, fileName: id, title: id, url: `${environment.apiUrl}/blobs/${id}`};
					}));
			}));
		}));
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	onFileUploaded(event) {
		this.fileItems = [
			{
				id: event.blobId,
				fileName: event.blobId,
				title: event.blobId,
				url: `${environment.apiUrl}/blobs/${event.blobId}`
			}, ...this.fileItems];
	}

	copyLink(imageUrl: string): void {
		this.clipboard.copy(imageUrl);
	}

	openImagePreview(imageUrl: string): void {
		const dialogRef = this.dialog.open(ImagePreviewDialogComponent, {
			maxWidth: "100%",
			maxHeight: "100%",
			data: { imageUrl: imageUrl }
		});

		// dialogRef.afterClosed().subscribe(result => {
		// 	console.log(`Dialog result: ${result}`);
		// });
	}
	
	deleteImage(image) {
		this.mediaService.deleteFile(image.id).subscribe(() => {
			this.fileItems = this.fileItems.filter(item => item.id !== image.id);
		});
	}
}

@Component({
	selector: 'betwelines-image-preview-dialog',
	template: `
		<h1 mat-dialog-title>Image Preview</h1>
		<div mat-dialog-content>
			<img src="{{data.imageUrl}}" style="width:50%; height:50%;" />
		</div>
		<div mat-dialog-actions>
			<button mat-button (click)="ok()">Close</button>
		</div>
	`,
})
export class ImagePreviewDialogComponent { 
	constructor(
		public dialogRef: MatDialogRef<ImagePreviewDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: {imageUrl: string}) {}

	ok(): void {
		this.dialogRef.close(ConfirmationDialogResult.OK);
	}
}