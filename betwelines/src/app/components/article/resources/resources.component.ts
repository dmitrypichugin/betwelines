import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../article';

@Component({
	selector: 'betwelines-resources',
	templateUrl: './resources.component.html',
	styleUrls: ['./resources.component.css']
})
export class ResourcesComponent implements OnInit {

	@Input() article: Article;

	constructor() { }

	ngOnInit(): void {
	}

}
