import { Injectable } from '@angular/core';
import { ReplaySubject, Observable } from 'rxjs';
import { Article } from './article';

@Injectable({
	providedIn: 'root'
})
export class ArticleStoreService {

	articleSubject: ReplaySubject<Article> = new ReplaySubject(1);
	articleObservable: Observable<Article> = this.articleSubject.asObservable();

	constructor() { }

	getSelectedArticle(): Observable<Article> {
		return this.articleObservable;
	}

	setSelectedArticle(article: Article): void {
		this.articleSubject.next(article);
	}
	
}
