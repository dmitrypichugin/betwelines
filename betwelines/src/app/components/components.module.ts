import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsComponent } from './cards/cards.component';
import { SharedModule } from '../shared/shared.module';
import { CardEditorComponent } from './cards/card-editor/card-editor.component';
import { VersionsComponent } from './versions/versions.component';
import { CardViewerComponent } from './cards/card-viewer/card-viewer.component';
import { SettingsModule } from './settings/settings.module';
import { HeaderComponent } from './header/header.component';
import { TocComponent } from './toc/toc.component';
import { ArticleComponent } from './article/article.component';
import { MainComponent } from './main/main.component';
import { VersionSelectorComponent } from './header/version-selector/version-selector.component';
import { CardListComponent } from './article/card-list/card-list.component';
import { ImagesPanelComponent } from './article/images-panel/images-panel.component';
import { ResourcesComponent } from './article/resources/resources.component';
import { NewArticleDialogComponent } from './new-article-dialog/new-article-dialog.component';
import { NewArticleButtonComponent } from './new-article-button/new-article-button.component';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { SearchComponent } from './search/search.component';
import { TextEditorComponent } from './text-editor/text-editor.component';
import { TextViewerComponent } from './text-viewer/text-viewer.component';

@NgModule({
	declarations: [
		CardsComponent,
		CardEditorComponent,
		VersionsComponent,
		CardViewerComponent,
		HeaderComponent,
		TocComponent,
		ArticleComponent,
		MainComponent,
		VersionSelectorComponent,
		CardListComponent,
		ImagesPanelComponent,
		ResourcesComponent,
		NewArticleDialogComponent,
		NewArticleButtonComponent,
		SearchComponent,
		TextEditorComponent,
		TextViewerComponent,
	],
	imports: [
		CommonModule,
		SharedModule,
		SettingsModule,
	],
	exports: [
		HeaderComponent,
	],
	entryComponents: [
		CardEditorComponent,
		CardViewerComponent,
		ImagesPanelComponent,
		ConfirmationDialogComponent,
	],
})
export class ComponentsModule { }
