import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import Editor from '@toast-ui/editor';
import { TextEditorService } from './text-editor.service';

export interface MarkdownData {
	html: string;
	markdown: string;
}

@Component({
	selector: 'betwelines-text-editor',
	templateUrl: './text-editor.component.html',
	styleUrls: ['./text-editor.component.css'],
	encapsulation: ViewEncapsulation.None
})
export class TextEditorComponent implements OnInit {

	@ViewChild('editor')
	editorElement: ElementRef;

	
	@Input()
	get editMode(): boolean {
		return this._editMode;
	}
	set editMode(mode: boolean) {
		this._editMode = mode;
		this.editor && this.editor
	}
	_editMode: boolean;

	@Input() options: object;
	editor: Editor;
	events = {
		change: this.changed.bind(this),
		load: this.loadedEditor.bind(this),
		blur: this.blur.bind(this),
	};

	@Output() loaded: EventEmitter<void> = new EventEmitter<void>();
	@Output() onChangeMarkdown: EventEmitter<string> = new EventEmitter<string>();
	@Output() onChangeHTML: EventEmitter<string> = new EventEmitter<string>();
	@Output() onChange: EventEmitter<MarkdownData> = new EventEmitter<MarkdownData>();
	@Output() onBlurMarkdown: EventEmitter<string> = new EventEmitter<string>();
	@Output() onBlurHTML: EventEmitter<string> = new EventEmitter<string>();
	@Output() onBlur: EventEmitter<MarkdownData> = new EventEmitter<MarkdownData>();

	constructor(private editorService: TextEditorService) { }

	public ngOnInit() {
		// this.getEditor();
	}

	public ngAfterViewInit() {
		this.getEditor();
	}

	/*async*/ getEditor() {
		// (this.options as any).viewer = !this.editMode;
		this.editor = /*await*/ this.editorService.createEditor({
			events: this.events,
			...this.options,
			el: this.editorElement.nativeElement,
		});
	}

	loadedEditor() {
		this.loaded.emit();
	}

	changed() {
		this.onChangeMarkdown.emit(this.editor.getMarkdown(/*(this.options as any).editorId*/));
		this.onChangeHTML.emit(this.editor.getHtml(/*(this.options as any).editorId*/));
		this.onChange.emit({
			html: this.editor.getHtml(/*(this.options as any).editorId*/),
			markdown: this.editor.getMarkdown(/*(this.options as any).editorId*/),
		});
	}

	blur() {
		this.onBlurMarkdown.emit(this.editor.getMarkdown(/*(this.options as any).editorId*/));
		this.onBlurHTML.emit(this.editor.getHtml(/*(this.options as any).editorId*/));
		this.onBlur.emit({
			html: this.editor.getHtml(/*(this.options as any).editorId*/),
			markdown: this.editor.getMarkdown(/*(this.options as any).editorId*/),
		});
	}

	public setMarkdown(markdown: string): void {
		// setTimeout(() => 
		this.editor.setMarkdown(markdown, (this.options as any).editorId)
		// , 1000);
	}

}