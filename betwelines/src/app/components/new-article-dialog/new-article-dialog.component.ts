import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Article } from '../article/article';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ArticleService } from '../article/article.service';
import { VersionsStoreService } from '../versions/versions-store.service';
import { ArticleStoreService } from '../article/article-store.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
	selector: 'betwelines-new-article-dialog',
	templateUrl: './new-article-dialog.component.html',
	styleUrls: ['./new-article-dialog.component.css']
})
export class NewArticleDialogComponent implements OnInit, OnDestroy {

	subscription: Subscription = new Subscription();
	version: string;
	parentArticle: Article;

	constructor(
		private articleService: ArticleService,
		private versionsStoreService: VersionsStoreService,
		private articleStoreService: ArticleStoreService,
		private dialogRef: MatDialogRef<NewArticleDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public article: Article,
		private router: Router,
	) { }

	ngOnInit(): void {
		this.subscription
			.add(this.versionsStoreService.getSelectedVersion().subscribe(version => this.version = version))
			.add(this.articleStoreService.getSelectedArticle().subscribe(article => this.parentArticle = article));
	}
	
	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}

	cancel(): void {
		this.dialogRef.close();
	}

	saveNewArticle(): void {
		if (this.parentArticle) {
			this.article.parentHandle = this.parentArticle.handle;
		}
		this.articleService.createArticle(this.version, this.article).subscribe(article => {
			this.router.navigate(['/docs', this.version, article.handle]);   // Select the new article
			this.versionsStoreService.setSelectedVersion(this.version);                // Trigger TOC refresh
			this.dialogRef.close();
		}, error => alert("error (TODO: show a nice message): " + JSON.stringify(error)));
	}

}
