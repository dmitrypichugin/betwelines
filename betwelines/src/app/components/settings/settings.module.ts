import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { VersionSettingsComponent } from './version-settings/version-settings.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { UsersComponent } from './users/users.component';
import { UserEditorComponent } from './users/user-editor/user-editor.component';

@NgModule({
	declarations: [
		SettingsComponent,
		VersionSettingsComponent,
		UsersComponent,
		UserEditorComponent,
	],
	imports: [
		CommonModule,
		SharedModule,
	],
	entryComponents: [
		UserEditorComponent,
	]
})
export class SettingsModule { }
