import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionSettingsComponent } from './version-settings.component';

describe('VersionSettingsComponent', () => {
  let component: VersionSettingsComponent;
  let fixture: ComponentFixture<VersionSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
