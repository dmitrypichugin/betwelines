import { Component, OnInit } from '@angular/core';
import { Version } from '../../versions/version';
import { VersionsService } from '../../versions/versions.service';

@Component({
	selector: 'betwelines-version-settings',
	templateUrl: './version-settings.component.html',
	styleUrls: ['./version-settings.component.css']
})
export class VersionSettingsComponent implements OnInit {

	versions: Array<Version>;
	editingNewVersion: boolean;

	constructor(private versionService: VersionsService) { }

	ngOnInit() {
		this.load();
	}
	
	private load() {
		this.versionService.getVersions().subscribe(versions => this.versions = versions);
	}

	addVersion() {
		this.editingNewVersion = true;
	}

	cancelNewVersion() {
		this.editingNewVersion = false;
	}

	createVersion(versionLabel: string) {
		this.versionService.createVersion({
			index: this.versions.length + 1, //TODO: do this on the server
			label: versionLabel}).subscribe(() => {
				this.editingNewVersion = false;
				this.load();
			});
	}

	updateVersion(version: Version) {
		this.versionService.updateVersion(version).subscribe(() => this.load());
	}

}
