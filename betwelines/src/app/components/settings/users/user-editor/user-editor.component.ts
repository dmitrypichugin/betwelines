import { Component, OnInit, Inject } from '@angular/core';
import { User } from 'src/app/core/security/user/user';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'betwelines-user-editor',
	templateUrl: './user-editor.component.html',
	styleUrls: ['./user-editor.component.css']
})
export class UserEditorComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<UserEditorComponent>,
		@Inject(MAT_DIALOG_DATA) public user: User) { }

	close(): void {
		this.dialogRef.close();
	}
	
	ngOnInit() {
	}

}
