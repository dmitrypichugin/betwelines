import { Injectable } from '@angular/core';
import { User } from 'src/app/core/security/user/user';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class UserService {
	
	constructor(private http: HttpClient) { }
	
	getUsers(): any {
		return this.http.get<Array<User>>(`${environment.apiUrl}/api/users`);
	}

	createUser(user: { index: number; label: string; }): any {
		return this.http.post<User>(`${environment.apiUrl}/api/users`, user);
	}

	updateUser(user: User): any {
		return this.http.put<User>(`${environment.apiUrl}/api/users/${user.id}`, user);
	}

	deleteUser(id: string): any {
		return this.http.delete<void>(`${environment.apiUrl}/api/users/${id}`);
	}

}
