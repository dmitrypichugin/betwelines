import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/security/user/user';
import { UserService } from './user.service';
import { MatDialog } from '@angular/material/dialog';
import { UserEditorComponent } from './user-editor/user-editor.component';

@Component({
	selector: 'betwelines-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

	users: Array<User>;
	editingNewUser: boolean;

	constructor(
		private dialog: MatDialog,
		private userService: UserService) { }

	ngOnInit() {
		this.load();
	}
	
	private load() {
		this.userService.getUsers().subscribe(users => this.users = users);
	}

	addUser() {
		const dialogRef = this.dialog.open(UserEditorComponent, {
			width: '600px',
			data: {}
		});
		dialogRef.beforeClosed().subscribe(result => {
			result && this.userService.createUser(result).subscribe(() => this.load());
		});
	}

	editUser(user: User) {
		const dialogRef = this.dialog.open(UserEditorComponent, {
			width: '600px',
			data: Object.assign({}, user)
		});
		dialogRef.beforeClosed().subscribe(result => {
			result && this.userService.updateUser(result).subscribe(() => this.load());
		});
	}

	deleteUser(user: User) {
		this.userService.deleteUser(user.id).subscribe(() => this.load());
	}

}
