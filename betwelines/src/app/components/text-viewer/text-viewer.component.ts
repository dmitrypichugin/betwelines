import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import Editor from '@toast-ui/editor';
import { TextEditorService } from '../text-editor/text-editor.service';

@Component({
	selector: 'betwelines-text-viewer',
	templateUrl: './text-viewer.component.html',
	styleUrls: ['./text-viewer.component.css']
})
export class TextViewerComponent implements OnInit {

	@ViewChild('viewer')
	viewerElement: ElementRef;

	@Input() options: object;
	@Input()
	get markdownString() { return this._markdown; }
	set markdownString(markdown: string) { 
		this._markdown = markdown;
		if (this.editor) {
			this.editor.setMarkdown(markdown, (this.options as any).editorId);
		}
	}
	_markdown: string;

	editor: Editor;

	constructor(private editorService: TextEditorService) { }

	ngOnInit(): void {
		// const viewer = new Viewer({
		// 	el: document.querySelector('#viewer'),
		// 	// height: '600px',
		// 	initialValue: '# hello'
		// });
		// viewer
		// // viewer.getHtml();
	}

	public ngAfterViewInit() {
		this.getEditor();
	}

	/*async*/ getEditor() {
		(this.options as any).viewer = true;
		this.editor = /*await*/ this.editorService.createEditor({
			...this.options,
			el: this.viewerElement.nativeElement,
		});
		this.editor.setMarkdown(this._markdown, (this.options as any).editorId);
	}
}
