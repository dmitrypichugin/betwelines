import { Component, OnInit, OnDestroy } from '@angular/core';
import { NewArticleDialogComponent } from '../new-article-dialog/new-article-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
	selector: 'betwelines-new-article-button',
	templateUrl: './new-article-button.component.html',
	styleUrls: ['./new-article-button.component.css']
})
export class NewArticleButtonComponent implements OnInit {

	constructor(private newArticleDialog: MatDialog) { }

	ngOnInit(): void {
	}
	
	createNewArticle(): void {
		const dialogRef = this.newArticleDialog.open(NewArticleDialogComponent, {
			width: '500px',
			data: { }
		});
		// dialogRef.afterClosed().subscribe(article => {
		// });
	}
	
}
