import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Article } from '../article/article';
import { ArticleService } from '../article/article.service';
import { VersionsStoreService } from '../versions/versions-store.service';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
	selector: 'betwelines-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {
	
	@ViewChild(MatMenuTrigger) searchResultPopupTrigger: MatMenuTrigger;

	version: string;
	articles: Array<Article> = [];
	page: number = 0;
	pageSize: number = 10;
	query: string = "";
	moreDisabled: boolean = true;
	subscription: Subscription = new Subscription();

	constructor(
		// private dialog: MatDialog,
		private articleService: ArticleService,
		private versionsStoreService: VersionsStoreService) { }

	ngOnInit() {
		this.subscription.add(this.versionsStoreService.getSelectedVersion().subscribe(version => {
			this.version = version;
		}));
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}

	search(query: string) {
		this.query = query;
		this.page = 0;
		this.articles = [];
		this.doSearch();
	}

	doSearch() {
		this.articleService.findArticles(this.version, this.query, this.page, this.pageSize).subscribe(articles => {
			this.moreDisabled = articles.length < this.pageSize;
			this.articles = [...this.articles, ...articles];
			this.searchResultPopupTrigger.openMenu();
		});
	}

	// view(card: Article) {
	// 	this.dialog.open(CardViewerComponent, {
	// 		width: '660px',
	// 		data: {version: this.version, cardName: card.handle}
	// 	});
	// }

	more() {
		this.page++;
		this.doSearch();
	}

}
