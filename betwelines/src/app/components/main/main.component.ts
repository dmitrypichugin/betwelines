import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { VersionsStoreService } from '../versions/versions-store.service';
import { ArticleStoreService } from '../article/article-store.service';

@Component({
	selector: 'betwelines-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {

	subscriptions: Subscription = new Subscription();

	constructor(
		private versionsStoreService: VersionsStoreService,
		private articleStoreService: ArticleStoreService,
		private activatedRoute: ActivatedRoute,
		private router: Router) { }

	ngOnInit() {
		this.subscriptions.add(this.activatedRoute.paramMap.subscribe(params => {
			const version = params.get('version');
			const articleHandle = params.get('articleHandle');
			if (!articleHandle) {
				this.articleStoreService.setSelectedArticle(null);
			}
			if (version) {
				this.versionsStoreService.setSelectedVersion(version);
			} else {
				this.subscriptions.add(this.versionsStoreService.getDefaultVersion().subscribe(
					defaultVersion => this.router.navigate(['docs', defaultVersion]) /*this.versionsStoreService.setSelectedVersion(defaultVersion)*/));
			}
		}));
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

}
