import { Component, OnInit } from '@angular/core';
import { TocService } from './toc.service';
import { TocTree, ArticleNode } from './toc';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { VersionsStoreService } from '../versions/versions-store.service';
import { Subscription } from 'rxjs';
import { ArticleStoreService } from '../article/article-store.service';

interface ExampleFlatNode {
	expandable: boolean;
	name: string;
	level: number;
	card: boolean;
}

@Component({
	selector: 'betwelines-toc',
	templateUrl: './toc.component.html',
	styleUrls: ['./toc.component.css']
})
export class TocComponent implements OnInit {

	version: string;

	subscriptions: Subscription = new Subscription();

	constructor(
		private tocService: TocService,
		private versionsStoreService: VersionsStoreService,
		private articleStoreService: ArticleStoreService) { }

	ngOnInit() {
		this.subscriptions.add(this.versionsStoreService.getSelectedVersion().subscribe(version => {
			this.version = version;
			if (version) {
				this.tocService.getTocTree(version).subscribe(toc => {
					this.dataSource.data = toc.nodes;
					this.subscriptions.add(this.articleStoreService.getSelectedArticle().subscribe(article => {
						article && this.expandParentNodes(article.handle);
					}));
				});
			}
		}));
	}

	private expandParentNodes(childArticleHandle: string): void {
		let handle = childArticleHandle;
		while (true) {
			const dataNode = this.allNodes.get(handle);
			if (!dataNode) {
				break;
			}
			this.treeControl.expand(dataNode);
			handle = dataNode.parentArticleHandle;
		}
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	private allNodes = new Map(); // Map<articleHandle, ArticleNode>

	// tmpCounter = 0; //TODO: remove this once all the glitchy nodes are removed from DB, otherwise we get tocLabel/handle as undefined and MatTree dies

	private _transformer = (node: ArticleNode, level: number) => {
		const dataNode = {
			expandable: !!node.nodes && node.nodes.length > 0,
			name: node.tocLabel, // || "" + ++this.tmpCounter, //node.name,
			// articleId: node.articleId,
			handle: node.handle, // || ""+ this.tmpCounter,
			level: level,
			parentArticleHandle: node.parentHandle,
			card: node.card,
			// children: node.nodes
		};
		this.allNodes.set(node.handle, dataNode);
		return dataNode;
	}

	treeControl = new FlatTreeControl<ExampleFlatNode>(
		node => node.level, node => node.expandable);

	treeFlattener = new MatTreeFlattener(
		this._transformer, node => node.level, node => node.expandable, node => node.nodes);

	dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

	hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

	isCard = (_: number, node: ExampleFlatNode) => node.card;

}
