export interface TocTree {
	id: string;
	nodes: ArticleNode[];
}

// export interface ArticleNode {
// 	articleId: string;
// 	name: string;      // Appears in the TOC tree
// 	handle: string;    // A normalazed version of name, to be used in the URL, for better direct links appearance.
// 	nodes: ArticleNode[];
// 	parentArticleHandle?: string;
// }
export interface ArticleNode {
	// articleId: string;
	tocLabel: string;      // Appears in the TOC tree
	handle: string;    // A normalazed version of name, to be used in the URL, for better direct links appearance.
	nodes?: ArticleNode[];
	parentHandle?: string;
	card: boolean;
}