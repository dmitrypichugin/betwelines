import { Injectable, Version } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TocTree } from './toc';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class TocService {

	constructor(private http: HttpClient) { }

	getTocTree(version: string): Observable<TocTree> {
		return this.http.get<TocTree>(`${environment.apiUrl}/api/betwelines/${version}/toc`);
	}

}
