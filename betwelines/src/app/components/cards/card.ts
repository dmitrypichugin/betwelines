import { CardTag } from "src/app/shared/card-tag/card-tag";

export interface Card {
	id: string,
	handle?: string,         // Human-readable unique ID, in case we need to reference a card

	title: string, //TODO: replaced by tocLabel
	tocLabel: string,
	parentHandle: string,
	content: string,
	tags?: Array<string>,
	tagObjects?: Array<CardTag>,
	// fields?: Array<Field>,

	// Fields that deal with versioning:
	sinceVersion?: number,   // This field is populated with the currently active version when the card is created.
	untilVersion?: number,   // This field is populated when 
	oid?: string,            // ID of the originating card - if we change a card in a future version a new copy
	                         // of the card gets created and this field will contain the ID of the original card.
	                         // For brand new cards this field is equal to the id field.

}

// export interface Field {
// 	type: number,
// 	label: string,
// 	value: string,
// }
