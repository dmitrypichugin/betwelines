import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Card } from './card';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Article } from '../article/article';

@Injectable({
	providedIn: 'root'
})
export class CardsService {

	constructor(private http: HttpClient) { }

	public getCardsByArticle(version: string, articleHandle: string): Observable<Array<Article>> {
		return this.http.get<Array<Article>>(`${environment.apiUrl}/api/betwelines/${version}/cards?articleHandle=${articleHandle}`);
	}

	public findCards(version: string, searchText: string, page: number = 0, pageSize: number = 10): Observable<Array<Card>> {
		return this.http.get<Array<Card>>(
			`${environment.apiUrl}/api/betwelines/${version}/cards/?searchText=${searchText}&from=${page * pageSize}&pageSize=${pageSize}`);
	}

	public getCard(version: string, id: string): Observable<Card> {
		return this.http.get<Card>(`${environment.apiUrl}/api/betwelines/${version}/cards/${id}`);
	}

	public createCard(version: string, card: Card): Observable<Card> {
		return this.http.post<Card>(`${environment.apiUrl}/api/betwelines/${version}/cards`, card);
	}

	public updateCard(version: string, card: Card) {
		return this.http.put<Card>(`${environment.apiUrl}/api/betwelines/${version}/cards/${card.id}`, card);
	}

	public deleteCard(version: string, id: string): Observable<void> {
		return this.http.delete<void>(`${environment.apiUrl}/api/betwelines/${version}/cards/${id}`);
	}

}
