import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Card } from './card';
import { CardEditorComponent } from './card-editor/card-editor.component';
import { CardsService } from './cards.service';
import { VersionsService } from '../versions/versions.service';
import { Version } from '../versions/version';
import { CardViewerComponent } from './card-viewer/card-viewer.component';
import { VersionsStoreService } from '../versions/versions-store.service';
import { Subscription } from 'rxjs';
import { ArticleStoreService } from '../article/article-store.service';

@Component({
	selector: 'betwelines-cards',
	templateUrl: './cards.component.html',
	styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit, OnDestroy {

	version: string;
	versions: Array<Version>;
	cards: Array<Card> = [];
	page: number = 0;
	pageSize: number = 10;
	query: string = "";
	moreDisabled: boolean = true;


	subscription: Subscription = new Subscription();


	constructor(
		private dialog: MatDialog,
		private cardsService: CardsService,
		private versionStoreService: VersionsStoreService,
		private articleStoreService: ArticleStoreService,) { }

	ngOnInit() {
		this.subscription.add(this.versionStoreService.getSelectedVersion().subscribe(version => {
			this.version = version;
			this.subscription.add(this.articleStoreService.getSelectedArticle().subscribe(article => {
				if (article) {
					this.cardsService.getCardsByArticle(version, article.handle).subscribe(cards => this.cards = cards);
				} else {
					this.cards = [];
				}
			}));
		}));
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}

	search(query: string) {
		this.query = query;
		this.page = 0;
		this.cards = [];
		this.doSearch();
	}

	doSearch() {
		this.cardsService.findCards(this.version, this.query, this.page, this.pageSize).subscribe(cards => {
			this.moreDisabled = cards.length < this.pageSize;
			this.cards = [...this.cards, ...cards];
		});
	}

	add() {
		const dialogRef = this.dialog.open(CardEditorComponent, {
			width: '600px',
			data: {}
		});
		dialogRef.beforeClosed().subscribe(result => {
			// result.tags = [... result.tags.split(",").map(tag => tag.trim())];
			result && this.cardsService.createCard(this.version, result).subscribe(card => this.cards = [card, ...this.cards]);
		});
	}

	edit(card: Card) {
		const dialogRef = this.dialog.open(CardEditorComponent, {
			width: '600px',
			data: Object.assign({}, card)
		});
		dialogRef.beforeClosed().subscribe(result => {
			result && this.cardsService.updateCard(this.version, result).subscribe(card => console.log("TODO: Update the search results..."));
		});
	}

	view(card: Card) {
		this.dialog.open(CardViewerComponent, {
			width: '660px',
			data: {version: this.version, cardName: card.handle}
		});
	}

	delete(card: Card) {
		this.cardsService.deleteCard(this.version, card.id).subscribe(card => console.log("TODO: Update the search results..."));
	}

	more() {
		this.page++;
		this.doSearch();
	}

}
