import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Card } from '../card';

@Component({
	selector: 'betwelines-card-editor',
	templateUrl: './card-editor.component.html',
	styleUrls: ['./card-editor.component.css']
})
export class CardEditorComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<CardEditorComponent>,
		@Inject(MAT_DIALOG_DATA) public card: Card) { }

	close(): void {
		this.dialogRef.close();
	}

	ngOnInit() {
	}

}
