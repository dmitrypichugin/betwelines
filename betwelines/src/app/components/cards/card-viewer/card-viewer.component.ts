import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Card } from '../card';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'betwelines-card-viewer',
	templateUrl: './card-viewer.component.html',
	styleUrls: ['./card-viewer.component.css']
})
export class CardViewerComponent implements OnInit {
	
	url: SafeUrl;

	constructor(
			public dialogRef: MatDialogRef<CardViewerComponent>,
			@Inject(MAT_DIALOG_DATA) public cardInfo: { version: number, cardName: string },
			sanitizer: DomSanitizer) {

		this.url = sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiUrl}/viewer/${cardInfo.version}/${cardInfo.cardName}`);
	}

	ngOnInit() {
	}

	close(): void {
		this.dialogRef.close();
	}

}
