import { Injectable } from '@angular/core';
import { ReplaySubject, Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class VersionsStoreService {

	versionSubject: ReplaySubject<string> = new ReplaySubject(1);
	versionObservable: Observable<string> = this.versionSubject.asObservable();

	//TODO: this is not a suitable type of subject:
	defaultVersionSubject: ReplaySubject<string> = new ReplaySubject(1);
	defaultVersionObservable: Observable<string> = this.defaultVersionSubject.asObservable();

	constructor() { }

	getSelectedVersion(): Observable<string> {
		return this.versionObservable;
	}

	setSelectedVersion(version: string): void {
		this.versionSubject.next(version);
	}

	getDefaultVersion(): Observable<string>  {
		return this.defaultVersionObservable;
	}

	setDefaultVersion(version: string): void {
		this.defaultVersionSubject.next(version);
	}

}
