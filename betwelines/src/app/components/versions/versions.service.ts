import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Version } from './version';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class VersionsService {

	constructor(private http: HttpClient) { }

	getVersions(): Observable<Array<Version>> {
		return this.http.get<Array<Version>>(`${environment.apiUrl}/api/betwelines/versions`);
	}

	getVersion(id: string): Observable<Version> {
		return this.http.get<Version>(`${environment.apiUrl}/api/betwelines/versions/${id}`);
	}

	createVersion(version: Version): Observable<Version> {
		return this.http.post<Version>(`${environment.apiUrl}/api/betwelines/versions`, version);
	}

	updateVersion(version: Version): Observable<Version> {
		return this.http.put<Version>(`${environment.apiUrl}/api/betwelines/versions/${version.id}`, version);
	}

	deleteVersion(id: string): Observable<void> {
		return this.http.delete<void>(`${environment.apiUrl}/api/betwelines/versions/${id}`);
	}

}
