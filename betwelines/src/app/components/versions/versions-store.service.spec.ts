import { TestBed } from '@angular/core/testing';

import { VersionsStoreService } from './versions-store.service';

describe('VersionsStoreService', () => {
  let service: VersionsStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VersionsStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
