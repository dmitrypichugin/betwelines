#!/bin/sh
#rm -rf ./data

DOCKER_COMPOSE_OPTIONS=""
if [ $1 -eq "--build" ]
then
	DOCKER_COMPOSE_OPTIONS = $DOCKER_COMPOSE_OPTIONS + "--build"
	# TODO: create a docker image that builds both back-end and front-end.
fi

mkdir -p ./data
docker-compose up $DOCKER_COMPOSE_OPTIONS

