package com.betwelines.api.article;

import java.util.Date;

import org.springframework.data.annotation.Id;

import com.betwelines.api.Indexable;

public class Article implements Indexable {

	@Id
	private String id;
	private String title;
	private String content;
	/** A hidden note that is displayed to the users that have permissions. */
	private String note;
	
	private String handle;  // : "my-mega-top-article", //what appears in the URL:
	// /betwelines/V.1.2.3/my-mega-top-article/my-sub-article
	private String parentHandle;
	
	private Boolean customHandle; // Is set to true by the user if they want to modify the handle. 
	          // TODO: Use article ID as a directory name, then we can allow users to change handles
	
	private String tocLabel; //: "My Mega Top Article",      //what appears in the TOC menu and in the breadcrumbs
	private Integer order;  // : 9, // might be used in the future to sort articles within the parent
	private Date created;   // : "2020-02-21T10:14:00.000Z", // ISO format (new Date().toISOString() in JavaScript)
	private Date updated;   // : "2020-02-21T10:14:00.000Z"

//	private String version; //TODO: versionSince would be a better name
	
	private Boolean card; // Indicates whether this article is a tooltip (card) and is not part of TOC

//	private List<Card> cards;

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title != null ? title : tocLabel != null ? tocLabel : null; //TODO: review
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

//	public List<Card> getCards() {
//		return cards;
//	}
//
//	public void setCards(List<Card> cards) {
//		this.cards = cards;
//	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public String getParentHandle() {
		return parentHandle;
	}

	public void setParentHandle(String parentHandle) {
		this.parentHandle = parentHandle;
	}

	public Boolean getCustomHandle() {
		return customHandle;
	}

	public void setCustomHandle(Boolean customHandle) {
		this.customHandle = customHandle;
	}

	public String getTocLabel() {
		return tocLabel;
	}

	public void setTocLabel(String tocLabel) {
		this.tocLabel = tocLabel;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Boolean getCard() {
		return card;
	}

	public void setCard(Boolean card) {
		this.card = card;
	}

}
