package com.betwelines.api.article;

import java.util.List;

public class ArticleNode {

	private String tocLabel;
	private String handle;
	private String parentHandle;
	private Boolean card;
	private List<ArticleNode> nodes;


	public List<ArticleNode> getNodes() {
		return nodes;
	}

	public void setNodes(List<ArticleNode> nodes) {
		this.nodes = nodes;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public String getParentHandle() {
		return parentHandle;
	}

	public void setParentHandle(String parentHandle) {
		this.parentHandle = parentHandle;
	}

	public String getTocLabel() {
		return tocLabel;
	}

	public void setTocLabel(String tocLabel) {
		this.tocLabel = tocLabel;
	}

	public Boolean getCard() {
		return card;
	}

	public void setCard(Boolean card) {
		this.card = card;
	}

}
