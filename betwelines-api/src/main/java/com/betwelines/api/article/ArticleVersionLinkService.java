package com.betwelines.api.article;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

@Service
public class ArticleVersionLinkService {

	@Autowired
	private ArticleVersionLinkRepository articleVersionLinkRepo;
	
	public void saveArticleVersionLink(ArticleVersionLink link/* String version, Article article */) {
//		ArticleVersionLink link = new ArticleVersionLink();
//		link.setArticleId(article.getId());
//		link.setHandle(article.getMeta().getHandle());
//		link.setVersion(version);
		articleVersionLinkRepo.save(link);
	}
	
	public List<ArticleVersionLink> findLinks(String version) {
		ArticleVersionLink probe = new ArticleVersionLink();
		probe.setVersion(version);
		return articleVersionLinkRepo.findAll(Example.of(probe));
	}
	
}
