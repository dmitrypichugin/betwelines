package com.betwelines.api.article;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.helper.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.betwelines.api.card.search.IndexService;
import com.betwelines.api.exception.AppLogicException;
import com.betwelines.api.exception.ExceptionUtil;

//@Profile("nogit")
@Service("articleServiceImpl")
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	protected ArticleRepository articleRepo;
	@Autowired
	protected ArticleVersionLinkRepository articleVersionLinkRepo;
	@Autowired
	private IndexService indexService;
	@Autowired
	private ArticleVersionLinkService articleVersionLinkService;

	
	@Override
	public Article getArticle(String articleId) {
		return articleRepo.findById(articleId).orElseThrow(() -> new AppLogicException("Article not found"));
	}

	@Override
	public Article getArticleByHandle(String version, String articleHandle) {
		
		ArticleVersionLink link = articleVersionLinkRepo.findByVersionAndHandle(version, articleHandle)
				.orElseThrow(() -> new AppLogicException("Article not found"));
		
		return articleRepo.findById(link.getArticleId())
				.orElseThrow(() -> new AppLogicException("Article not found"));
	}

	/**
	 * This method is called when we create a new article on the UI. The article
	 * body is empty (null) at this point.
	 */
	@Override
	public Article addArticle(String version, Article article) {
		
		adjustArticleHandle(version, article);

		articleRepo.save(article);
		
		articleVersionLinkService.saveArticleVersionLink(
				new ArticleVersionLink(
						article.getId(), 
						version,
						article.getHandle()));
		
		indexService.createIndex(article, version);

		return article;
	}

	@Override
	public Article updateArticle(String version, Article article) {
		
		ArticleVersionLink link = buildLink(article.getId(), article.getHandle());
		
		List<ArticleVersionLink> linksToArticle = articleVersionLinkRepo.findAll(Example.of(link));
		
		if (linksToArticle.size() > 1) {
			
			ArticleVersionLink ownLink = linksToArticle.stream()
				.filter(l -> version.equals(l.getVersion()))
				.findFirst().get();
			
			// Remove the link to the original article:
			articleVersionLinkRepo.deleteByVersionAndArticleId(ownLink.getVersion(), ownLink.getArticleId());
			
			// Create a new copy of the article to leave alone the original one:
			article.setId(null);
			article = addArticle(version, article);
			
		} else {
			articleRepo.save(article);
		}
		return article;
	}

	@Override
	public void deleteArticle(String version, String articleId) {
		
		Article article = articleRepo.findById(articleId)
				.orElseThrow(() -> new AppLogicException("Article not found"));
		
		ArticleVersionLink link = buildLink(articleId, article.getHandle());
		
		if (articleVersionLinkRepo.count(Example.of(link)) <= 1) {
			articleRepo.deleteById(articleId);
		}
		link.setVersion(version);
		articleVersionLinkRepo.deleteByVersionAndArticleId(version, articleId);

		indexService.deleteIndex(articleId);
		
		//TODO: Delete child articles
	}

	private ArticleVersionLink buildLink(String articleId, String handle) {
		ArticleVersionLink link = new ArticleVersionLink();
		link.setArticleId(articleId);
		link.setHandle(handle);
		return link;
	}
	
	@Override
	public List<Article> findArticles(String version, String query, Integer from, Integer pageSize) {
		List<String> ids = indexService.find(version, query, from, pageSize);
		List<Article> cardList = new ArrayList<>();
		for (String id : ids) {
			Article article = getArticle(id);
			ExceptionUtil.throwLogicIfNull(article, "Document was found in index but doesn't exist in DB: " + id);
			cardList.add(article);
		}
		return cardList;
	}
	
	
	private void adjustArticleHandle(final String version, final Article article) {
		
		if (!Boolean.TRUE.equals(article.getCustomHandle())) {
			
			String handle = titleToHandle(article.getTocLabel());
			String handleX = handle;
			int x = 1;
			while (!isHandleUnique(handleX, version)) {
				handleX = handle + "-" + x++;
			}
			article.setHandle(handleX);
		}
	}
	
	private String titleToHandle(String title) {
		ExceptionUtil.throwLogicIfTrue(StringUtil.isBlank(title), "Article title is mandatory.");
		
		String handle = title.trim()
				.replaceAll("\\-+", " ")  // Temporary replace dashes with spaces
				.replaceAll("\\W+", " ")  // Replace non-word characters with spaces (anything other than [a-zA-Z_0-9])
				.trim()
				.replaceAll("\\s+", "-")  // Replace whitespaces with dashes
				.replaceAll("\\-+", "-")  // Replace multiple dashes with a single dash
				.toLowerCase();
		
		return handle;
	}
	
	private boolean isHandleUnique(String handle, String version) {
		ArticleVersionLink link = new ArticleVersionLink();
		link.setHandle(handle);
//		link.setVersion(version); // TODO: not sure if it should be unique within a version or globally
		return articleVersionLinkRepo.count(Example.of(link)) == 0;
	}

}
