package com.betwelines.api.article;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArticleController {
	
	@Autowired
	@Qualifier("articleServiceImpl")
	private ArticleService articleService;

	
	@RequestMapping(path = "/api/betwelines/{version}/articles", method = RequestMethod.GET)
	public List<Article> findArticles(@PathVariable("version") String version,
			@RequestParam("searchText") String searchText,
			@RequestParam(value = "from", required = false) Integer from,
			@RequestParam(value = "pageSize", required = false) Integer pageSize) {
		return articleService.findArticles(version, searchText, from, pageSize);
	}
	
	@GetMapping(value = "/api/betwelines/{version}/articles", params = {"handle"})
	public Article getArticleByHandle(
			@PathVariable("version") String version,
			@RequestParam("handle") String articleHandle) {
		return articleService.getArticleByHandle(version, articleHandle);
	}

	@GetMapping("/api/betwelines/{version}/articles/{id}")
	public Article getArticle(
			@PathVariable("version") String version,
			@PathVariable("id") String articleId) {
		return articleService.getArticle(articleId);
	}

	@PostMapping("/api/betwelines/{version}/articles")
	public Article addArticle(
			@PathVariable("version") String version,
			@RequestBody Article article) {
		return articleService.addArticle(version, article);
	}

	@PutMapping("/api/betwelines/{version}/articles/{id}")
	public Article updateArticle(
			@PathVariable("version") String version,
			@PathVariable("id") String articleId,
			@RequestBody Article article) {
		article.setId(articleId);
		return articleService.updateArticle(version, article);
	}

	@DeleteMapping("/api/betwelines/{version}/articles/{id}")
	public void deleteArticle(
			@PathVariable("version") String version,
			@PathVariable("id") String articleId) {
		articleService.deleteArticle(version, articleId);
	}

}
