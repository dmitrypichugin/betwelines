package com.betwelines.api.article;

public class ArticleVersionLink {

	private String articleId;
	private String version;
	private String handle;
	
	public ArticleVersionLink() {}
	

	public ArticleVersionLink(String articleId, String version, String handle) {
		this.articleId = articleId;
		this.version = version;
		this.handle = handle;
	}


	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

}
