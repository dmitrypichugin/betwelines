package com.betwelines.api.article;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends MongoRepository<Article, String> {

//	@Query(
//			value="{ 'version' : ?0 }",
//			fields="{ 'version': 1, 'handle': 1, 'parentHandle': 1, 'tocLabel': 1}")
//	List<ArticleNode> findByVersion(String version);

	@Query(
			value="{ '_id' : { $in: ?0 }}",
			fields="{ 'handle': 1, 'parentHandle': 1, 'tocLabel': 1, 'card': 1}")
	List<ArticleNode> findById(List<String> ids);
	
	List<Article> findByParentHandle(String parentHandle);

}
