package com.betwelines.api.article;

import java.util.List;

public interface ArticleService {

	Article getArticle(String articleId);

	Article getArticleByHandle(String version, String articleHandle);

	/**
	 * This method does everything related to the article creation:
	 * <pre>
	 * - adds new record to DB;
	 * - adds the directory structure in the Git repository;
	 * - commits the changes to Git
	 * </pre>
	 */
	Article addArticle(String version, Article article);

	Article updateArticle(String version, Article article);

	void deleteArticle(String version, String articleId);

	List<Article> findArticles(String version, String query, Integer from, Integer pageSize);
	
}
