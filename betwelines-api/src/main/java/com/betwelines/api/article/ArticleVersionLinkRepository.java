package com.betwelines.api.article;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleVersionLinkRepository extends MongoRepository<ArticleVersionLink, String> {

	List<ArticleVersionLink> findByVersion(String version);
	Optional<ArticleVersionLink> findByVersionAndHandle(String version, String handle);
	
	// We're not using data-jpa, so we can't delete by a composite key (@IdClass)
	// and have to resort to explicit query:
//	@DeleteQuery(value="{'version' : $0, 'articleId' : $1}")
	void deleteByVersionAndArticleId(String version, String articleId); //ArticleVersionLink link);
	
}
