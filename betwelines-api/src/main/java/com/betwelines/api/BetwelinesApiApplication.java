package com.betwelines.api;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.betwelines.api.security.SecurityConfig;
import com.betwelines.api.security.User;
import com.betwelines.api.security.UserService;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.DefaultApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BetwelinesApiApplication {

	public static void main(String[] args) {
		System.out.println("Betwelines API is starting with arguments: " + Arrays.asList(args));
		waitFor(args);
		SpringApplication.run(BetwelinesApiApplication.class, args);
	}
	
	private static void waitFor(String[] args) {
		List<String> urls = new DefaultApplicationArguments(args).getOptionValues("waitFor");
		if (urls == null || urls.isEmpty()) {
			return;
		}
		System.out.println("Wait for: " + urls);
		while (!urls.isEmpty()) {
			for (Iterator<String> it = urls.iterator(); it.hasNext(); ) {
				String urlString = it.next();
				System.out.println("Waiting for " + urlString);
				URL url;
				try {
					url = new URL(urlString);
					HttpURLConnection con = (HttpURLConnection) url.openConnection();
					con.setRequestMethod("GET");
					int responseCode = con.getResponseCode();
					con.disconnect();
					it.remove();
					System.out.println(" - responded: " + responseCode);
				} catch (IOException e) {
					System.out.println(" - " + e.getMessage());
					try { Thread.sleep(5000); } catch (InterruptedException e1) {}
					break;
				}
			}
		}
	}
	
	@Bean
	public CommandLineRunner demo(
			UserService userService,
			RepoManager repoManager
			// GitRepoImporter dirReader,
			// VersionService versionService,
			// ArticleService articleService,
			// CardService cardService,
			// MediaStore mediaStore
			/*RestHighLevelClient restHighLevelClient*/) {
		return (args) -> {

			// Cleanup users before creating a new one:
			for (User user : userService.findAll()) {
				userService.delete(user.getId());
			}
			// Create an administrator user:
			userService.save(new User("Administrator", "admin", "0", Arrays.asList(new String[] {SecurityConfig.ROLE_USER, SecurityConfig.ROLE_ADMIN})));
			// Create a regular Joe user:
			userService.save(new User("user", "user@c.com", "0", Arrays.asList(new String[] {SecurityConfig.ROLE_USER})));
			

//			repoManager.reloadRepository();
			// mediaStore.deleteAll();
			// cardService.deleteAllCards();
			// versionService.deleteAllVersions();
			// dirReader.importAllVersions();
			
			
			
//			for (int i = 1; i < 5; i++) {
//				versionService.saveVersion(new Version((short) i, "Version " + String.format("%d", i)));
//			}
//			for (int i = 1; i < 100; i++) {
//				String s = String.format("%03d", i);
//				cardService.createCard((short) 1, new Card("name_" + s, "Title " + s, "Mega content " + s, null));
//			}
		};
	}
}
