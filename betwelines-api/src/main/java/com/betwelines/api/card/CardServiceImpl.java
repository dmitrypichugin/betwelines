package com.betwelines.api.card;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.betwelines.api.article.Article;
import com.betwelines.api.article.ArticleServiceImpl;

@Service("cardServiceImpl")
public class CardServiceImpl extends ArticleServiceImpl implements CardService {

	
	@Override
	public Card createCard(String version, Card card) {
		card.setCard(Boolean.TRUE);
		card.setTitle(card.getTocLabel()); //TODO: eventually get rid of title (or tocLabel)
		addArticle(version, card);
		return card;
	}
	
	@Override
	public Article getCard(String id) {
		return getArticle(id);
	}

	@Override
	public Card updateCard(String version, Card card) {
		card.setCard(Boolean.TRUE);
		card.setTitle(card.getTocLabel()); //TODO: eventually get rid of title (or tocLabel)
		updateArticle(version, card);
		return card;
	}

	@Override
	public void deleteCard(String version, String id) {
		deleteArticle(version, id);
	}

	@Override
	public List<Article> findCards(String version, String query, Integer from, Integer pageSize) {
		return findArticles(version, query, from, pageSize);
	}

	@Override
	public Article getCardByName(String version, String cardName) {
		return getArticleByHandle(version, cardName);
	}

	@Override
	public void deleteAllCards() {
		throw new RuntimeException("All cards deletion is not implemented");
	}
	
	/**
	 * This method is only needed if we make a separate call to get the list of all cards under an article.
	 * */
	@Override
	public List<Article> getArticleCards(String version, String parentArticleHandle) {
		//TODO: Not very optimal way of finding cards under an article
		return articleRepo.findByParentHandle(parentArticleHandle)
				.stream()
				.filter(card -> Boolean.TRUE.equals(card.getCard()) 
						&& articleVersionLinkRepo.findByVersionAndHandle(version, card.getHandle()).isPresent())
				.collect(Collectors.toList());
	}

}
