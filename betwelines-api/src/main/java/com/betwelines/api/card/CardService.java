package com.betwelines.api.card;

import java.util.List;

import com.betwelines.api.article.Article;

public interface CardService {

	List<? extends Article> findCards(String version, String criteria, Integer from, Integer pageSize);

	Card createCard(String version, Card card);
	
	Article getCard(String id);
	
	Card updateCard(String version, Card card);
	
	void deleteCard(String version, String id);

	Article getCardByName(String version, String cardName);

	void deleteAllCards();

	List<Article> getArticleCards(String version, String parentArticleHandle);

}
