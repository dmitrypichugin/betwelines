package com.betwelines.api.card.search;

import java.util.List;

import com.betwelines.api.Indexable;

public interface IndexService {

	Indexable updateIndex(Indexable indexable, String version);

	Indexable createIndex(Indexable indexable, String version);
	
	void deleteIndex(String id);

	List<String> find(String version, String query, Integer from, Integer pageSize);

}
