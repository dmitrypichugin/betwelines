package com.betwelines.api.card;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.betwelines.api.article.Article;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.options.MutableDataSet;

@RestController
public class CardViewerController {

	@Autowired
	@Qualifier("cardServiceImpl")
	private CardService cardService;

	@GetMapping(path = "/viewer/{version}/{name}") //, produces = {HTML}
	public StreamingResponseBody getCardByNameAsHtml(
			@PathVariable("version") String version,
			@PathVariable("name") String cardName,
			HttpServletRequest req) {
		
		System.out.println("VIEWER: " + req.getRemoteHost() + ", " + req.getRemoteAddr());
		
		Article card = cardService.getCardByName(version, cardName);
		
		return new StreamingResponseBody() {
			@Override
			public void writeTo(OutputStream outputStream) throws IOException {
//				outputStream.write(("<html><body>hello: " + Processor.process(card.getContent()) + "</html").getBytes()); //TODO: convert markdown to html
				
				MutableDataSet options = new MutableDataSet();

				// uncomment to set optional extensions
				//options.set(Parser.EXTENSIONS, Arrays.asList(TablesExtension.create(), StrikethroughExtension.create()));

				// uncomment to convert soft-breaks to hard breaks
				//options.set(HtmlRenderer.SOFT_BREAK, "<br />\n");

				Parser parser = Parser.builder(options).build();
				HtmlRenderer renderer = HtmlRenderer.builder(options).build();

				// You can re-use parser and renderer instances
				Node document = parser.parse(card.getContent());
				String html = renderer.render(document);
				outputStream.write(("<html><body>" + html + "</body></html>").getBytes());
			}
		};
	}

}
