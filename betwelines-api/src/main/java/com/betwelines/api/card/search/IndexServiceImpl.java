package com.betwelines.api.card.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.betwelines.api.Indexable;
import com.betwelines.api.exception.AppSystemException;
import com.betwelines.api.index.IndexReaderService;
import com.betwelines.api.index.IndexWriterService;

@Service
public class IndexServiceImpl implements IndexService {
	@Autowired
	private IndexWriterService indexWriterService;
	@Autowired
	private IndexReaderService indexReaderService;
	
	private Document buildDocument(Indexable indexable, String version) {
		Document document = new Document();

System.out.println("### Building a document");

		document.add(new StringField("id", indexable.getId(), Field.Store.YES));
		document.add(new TextField("title", indexable.getTitle(), Field.Store.YES));
		document.add(new TextField("body", emptify(indexable.getContent()), Field.Store.NO)); // We only want to index the body, but not store it
		document.add(new StringField("version", version, Field.Store.YES));
		return document;
	}

	private String emptify(String s) {
		return s == null ? "" : s;
	}
	
	@Override
	public Indexable createIndex(Indexable indexable, String version) {
		indexWriterService.addDocument(buildDocument(indexable, version));
		return indexable; //TODO: return void
	}
	
	@Override
	public Indexable updateIndex(Indexable indexable, String version) {
		indexWriterService.updateDocument(new Term("id", indexable.getId()),
				buildDocument(indexable, version));
		return indexable;
	}

	@Override
	public void deleteIndex(String indexableId) {
		indexWriterService.deleteDocument(new Term("id", indexableId));
	}
	
	@Override
	public List<String> find(String version, String queryString, 
			Integer from, Integer pageSize) {
		
		if (!StringUtils.hasText(queryString)) {
			return Collections.emptyList();
		}
		try {
			QueryParser qp = new MultiFieldQueryParser(new String[] { "title", "body" },
					new StandardAnalyzer());
			Query query = qp.parse(queryString);
			
			BooleanQuery fullQuery = new BooleanQuery.Builder()
					.add(query, Occur.MUST)
					.add(new TermQuery(new Term("version", version)), Occur.MUST)
//					.add(IntPoint.newRangeQuery("sinceVersion", 
//							Integer.MIN_VALUE, version), Occur.MUST)
//					.add(IntPoint.newRangeQuery("untilVersion",
//							version, Integer.MAX_VALUE), Occur.MUST)
					.build();
			
			List<Document> docs = indexReaderService.searchIndex(
					fullQuery, pageSize, from);
			
			List<String> indexablesIds = new ArrayList<>();
			for (Document doc : docs) {
				indexablesIds.add(doc.get("id"));
			}
			return indexablesIds;
		} catch (ParseException e) {
			throw new AppSystemException("Failed to parse the query.", e);
		}
	}
	
}
 