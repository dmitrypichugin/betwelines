package com.betwelines.api.card;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.betwelines.api.article.Article;
import com.betwelines.api.security.SecurityConfig;

@RestController
@Secured({SecurityConfig.ROLE_USER})
public class CardController {
	
	@Autowired
	@Qualifier("cardServiceImpl")
	private CardService cardService;

	
	@GetMapping(value = "/api/betwelines/{version}/cards", params = {"articleHandle"})
	public List<Article> getArticleCards(
			@PathVariable("version") String version,
			@RequestParam("articleHandle") String articleHandle) {
		return cardService.getArticleCards(version, articleHandle);
	}

	@RequestMapping(path = "/api/betwelines/{version}/cards", method = RequestMethod.GET)
	public List<? extends Article> findCards(@PathVariable("version") String version,
			@RequestParam("searchText") String searchText,
			@RequestParam(value = "from", required = false) Integer from,
			@RequestParam(value = "pageSize", required = false) Integer pageSize) {
		return cardService.findCards(version, searchText, from, pageSize);
	}

	@RequestMapping(path = "/api/betwelines/{version}/cards", method = RequestMethod.POST)
	public Card createCard(@PathVariable("version") String version, @RequestBody Card card) {
		return cardService.createCard(version, card);
	}
	
	@RequestMapping(path = "/api/betwelines/{version}/cards/{id}", method = RequestMethod.GET)
	public Article getCard(@PathVariable("version") String version, @PathVariable("id") String id) {
		return cardService.getCard(id);
	}
	
	@RequestMapping(path = "/api/betwelines/{version}/cards/{id}", method = RequestMethod.PUT)
	public Card updateCard(@PathVariable("version") String version, @PathVariable("id") String id, @RequestBody Card card) {
		return cardService.updateCard(version, card);
	}
	
	@RequestMapping(path = "/api/betwelines/{version}/cards/{id}", method = RequestMethod.DELETE)
	public void deleteCard(@PathVariable("version") String version, @PathVariable("id") String id) {
		cardService.deleteCard(version, id);
	}
	
}
