package com.betwelines.api.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {
	
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = userRepo.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Invalid username or password."));
		return new UserPrincipal(user.getId(), user.getEmail(), user.getPassword(), getAuthority(user.getRoles()));
	}

	private List<GrantedAuthority> getAuthority(List<String> roleNames) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (String role : roleNames) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

	public List<User> findAll() {
		List<User> list = new ArrayList<>();
		userRepo.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public User save(User user) {
		if (user.getPassword() != null) {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
		}
		return userRepo.save(user);
	}

	@Override
	public void delete(String id) {
		userRepo.deleteById(id);
	}

	@Override
	public User get(String id) {
		return userRepo.findById(id).orElseThrow(() -> new RuntimeException("User not found."));
	}
	
}
