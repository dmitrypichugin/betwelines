package com.betwelines.api.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class UserPrincipal extends org.springframework.security.core.userdetails.User {
	
	private static final long serialVersionUID = 20200328L;

	private String id;
	
	public UserPrincipal(String id, String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

}