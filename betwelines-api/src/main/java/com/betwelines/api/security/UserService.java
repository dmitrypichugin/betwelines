package com.betwelines.api.security;

import java.util.List;

public interface UserService {

//	User loadUserByUsername(String email) throws UsernameNotFoundException;
	
	User get(String id);
	
	List<User> findAll();
	
	User save(User user);

	void delete(String id);
	
}
