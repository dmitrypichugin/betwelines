package com.betwelines.api.security;

import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;


//@Entity
public class User {
	@Id
	private String id;

	@NotBlank
	@Size(max = 40)
	private String name;

	@NotBlank
	@Size(max = 40)
	@Email
	private String email;

	@NotBlank
	@Size(max = 100)
	@JsonProperty(access = Access.WRITE_ONLY) // We don't want to serialize the password, while deserialization is fine
	private String password;
	
	private List<String> roles;

	
	public User() {
	}

	public User(String name, String email, String password) {
		this(name, email, password, Arrays.asList(new String[] { SecurityConfig.ROLE_USER }));
	}
	
	public User(String name, String email, String password, List<String> roles) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.setRoles(roles);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

}
