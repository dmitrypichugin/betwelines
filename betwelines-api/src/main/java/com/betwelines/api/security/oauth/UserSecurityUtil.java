package com.betwelines.api.security.oauth;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

@Service
public class UserSecurityUtil {

	@Autowired
	private TokenStore tokenStore;
	
	public String getUserId(OAuth2Authentication authentication) {
		OAuth2AuthenticationDetails auth2AuthenticationDetails = (OAuth2AuthenticationDetails) authentication.getDetails();
		Map<String, Object> additionalInfo = tokenStore.readAccessToken(
				auth2AuthenticationDetails.getTokenValue()).getAdditionalInformation();
		return (String) additionalInfo.get("uid");
	}
	
}
