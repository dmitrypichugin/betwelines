package com.betwelines.api.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.betwelines.api.security.oauth.UserSecurityUtil;

@RestController
@RequestMapping("/api/users")
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	private UserSecurityUtil userSecurityUtil;
	

	@Secured({ SecurityConfig.ROLE_ADMIN, SecurityConfig.ROLE_USER })
	@RequestMapping(value = "/current", method = RequestMethod.GET)
	public User getCurrentUser(OAuth2Authentication authentication) {
		return userService.get(userSecurityUtil.getUserId(authentication));
	}

	@Secured(SecurityConfig.ROLE_ADMIN)
	@RequestMapping(method = RequestMethod.GET)
	public List<User> getAllUsers() {
		return userService.findAll();
	}

	@Secured(SecurityConfig.ROLE_ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public User register(@RequestBody User user) {
		return userService.save(user);
	}

	@Secured({SecurityConfig.ROLE_USER})
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public User update(@RequestBody User user, OAuth2Authentication authentication) {
		String currentUserId = userSecurityUtil.getUserId(authentication);
		if (!currentUserId.equals(user.getId()) 
				&& !authentication.getAuthorities().contains(new SimpleGrantedAuthority(SecurityConfig.ROLE_ADMIN))) {
			throw new RuntimeException("Not authorized to update the user");
		}
		return userService.save(user);
	}

	@Secured({SecurityConfig.ROLE_ADMIN})
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "id") String id) {
		userService.delete(id);
		return "success";
	}

}
