package com.betwelines.api;

public interface Indexable {

	String getId();
	
	String getTitle();
	
	String getContent();
	
}
