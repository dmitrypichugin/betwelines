package com.betwelines.api.config;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.betwelines.api.exception.AppSystemException;

@Configuration
public class LuceneConfig {
	
	//TODO: Move this to a resource property file:
	private String INDEX_PATH = "./lucene-index";

	@Bean
	public Directory memoryIndex() {
		try {
			return new MMapDirectory(Paths.get(INDEX_PATH));
		} catch (IOException e) {
			throw new AppSystemException("Failed to build a Lucene index directory " 
					+ new File(INDEX_PATH).getAbsolutePath(), e);
		}
	}
	
//	@Bean
//	public IndexWriterConfig indexWriterConfig() {
//		StandardAnalyzer analyzer = new StandardAnalyzer();
//		return new IndexWriterConfig(analyzer);
//	}

}
