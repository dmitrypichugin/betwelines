package com.betwelines.api.exception;

public class ExceptionUtil {
	
	public static void throwLogicIfNull(Object obj, String message) {
		if (obj == null) {
			throw new AppLogicException(message);
		}
	}
	
	public static void throwLogicIfTrue(boolean condition, String message) {
		if (condition) {
			throw new AppLogicException(message);
		}
	}

	public static void throwIllegalArgumentIfNull(Object obj, String message) {
		if (obj == null) {
			throw new IllegalArgumentException(message);
		}
	}

}
