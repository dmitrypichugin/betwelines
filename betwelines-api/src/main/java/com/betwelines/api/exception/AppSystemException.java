package com.betwelines.api.exception;

/**
 * This exception is thrown by the application when it catches a low level
 * exception. The purpose is to provide a helpful explanation in the message and
 * log the stacktrace. The message may contain technical details so we don't
 * want to expose it to the end-user.
 */
public class AppSystemException extends RuntimeException {

	public AppSystemException(String message, Throwable cause) {
		super(message, cause);
	}

}
