package com.betwelines.api.exception;

/**
 * This exception is thrown by the application when the application
 * decides that the condition is unacceptable due to various reasons.
 * The intention here is to provide a user-friendly explanation, but
 * unlike AppSystemException we don't want to log the stacktrace as
 * the application is the initiator of this exception. This is the 
 * reason we don't wrap another exception as a cause, hense the constructor.
 * */
public class AppLogicException extends RuntimeException {

	public AppLogicException(String message) {
		super(message);
	}

}
