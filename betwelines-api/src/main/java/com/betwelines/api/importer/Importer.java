package com.betwelines.api.importer;

public interface Importer {

	void importVersion(String version);

	void importAllVersions();

}
