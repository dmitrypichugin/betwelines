package com.betwelines.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.betwelines.api.card.CardService;
import com.betwelines.api.media.MediaStore;
import com.betwelines.api.version.VersionService;

/**
 * RepoManager
 */
@Service
public class RepoManager {

	@Autowired
	private VersionService versionService;
	@Autowired
	private CardService cardService;
	@Autowired
	private MediaStore mediaStore;

	public void reloadRepository() {
//		gitService.initRepoIfNotExists();
		mediaStore.deleteAll();
		cardService.deleteAllCards();
		versionService.deleteAllVersions();
//		dirReader.importAllVersions();
	}
	
}