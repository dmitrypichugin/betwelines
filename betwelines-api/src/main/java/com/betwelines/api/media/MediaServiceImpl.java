package com.betwelines.api.media;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.betwelines.api.exception.AppLogicException;
import com.betwelines.api.exception.AppSystemException;
import com.betwelines.api.exception.ExceptionUtil;

@Service
public class MediaServiceImpl implements MediaService {

	@Value("${media.store.path}")
	private String mediaStorePath;
	
	@Autowired
	private MediaDescriptorRepository mediaDescriptorRepo;
	
	@Override
	public MediaDescriptor saveMediaDescriptor(MediaDescriptor mediaDescriptor) {
		MediaDescriptor persisted = null;
		if (!StringUtils.isEmpty(mediaDescriptor.getArticleId())) {
			persisted = mediaDescriptorRepo.findByVersionAndArticleIdAndBlobId(
					mediaDescriptor.getVersion(),
					mediaDescriptor.getArticleId(),
					mediaDescriptor.getBlobId());
		} else if (!StringUtils.isEmpty(mediaDescriptor.getCardId())) {
			persisted = mediaDescriptorRepo.findByVersionAndCardIdAndBlobId(
					mediaDescriptor.getVersion(),
					mediaDescriptor.getArticleId(),
					mediaDescriptor.getBlobId());
		} else {
			throw new AppLogicException(
					"Trying to save a media descriptor without specifying an article or card ID: " + mediaDescriptor);
		}
		if (persisted != null) {
			// If the descriptor is found we'll use its ID so that the subsequent save()
			// would update the existing record instead of creating a brand new record:
			mediaDescriptor.setId(persisted.getId());
		}
		return mediaDescriptorRepo.save(mediaDescriptor);
	}

	@Override
	public void deleteMediaDescriptor(String id) {
		mediaDescriptorRepo.deleteById(id);
	}

	@Override
	public String saveBlob(InputStream inputStream) {
		String id = UUID.randomUUID().toString();
		
		String blobsDir = mediaStorePath + "/blobs";
		new File(blobsDir).mkdirs();
		
		try {
			Files.copy(inputStream, new File(blobsDir + "/" + id).toPath());
		} catch (IOException e) {
			throw new AppSystemException("Failed to store the blob file.", e);
		}
		return id;
	}
	
	@Override
	public void deleteBlob(String id) {
		
		deleteMediaDescriptor(id);
		
		File blobFile = new File(mediaStorePath + "/blobs/" + id);
		if (blobFile.exists()) {
			blobFile.delete();
		} else {
			throw new AppLogicException("The file has already been deleted.");
		}
	}

	@Override
	public String saveBlob(byte[] bytes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InputStream getBlobStream(String blobId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MediaDescriptor> getArticleMediaDescriptors(String version, String articleId) {
		return mediaDescriptorRepo.findByVersionAndArticleId(version, articleId);
	}

	@Override
	public List<MediaDescriptor> getCardMediaDescriptors(String version, String cardId) {
		return mediaDescriptorRepo.findByVersionAndCardId(version, cardId);
	}

	/** @deprecated Use saveMediaFile(MultipartFile file) instead. */
	@Override
	public MediaDescriptor saveMediaFile(String version, String articleId, MultipartFile file) {
		String blobId;
		try {
			blobId = saveBlob(file.getInputStream());
		} catch (IOException e) {
			throw new AppSystemException("Failed to read from uploaded input stream.", e);
		}
		MediaDescriptor mediaDescriptor = new MediaDescriptor();
		mediaDescriptor.setVersion(version);
		mediaDescriptor.setArticleId(articleId);
		mediaDescriptor.setBlobId(blobId);
		return saveMediaDescriptor(mediaDescriptor);
	}
	
	@Override
	public MediaBlob saveMediaFile(MultipartFile file) {
		ExceptionUtil.throwIllegalArgumentIfNull(file, "Invalid attempt to save a file, the file object is empty.");
		try {
			String uuid = saveBlob(file.getInputStream());
			return new MediaBlob(uuid, MediaTypeEnum.IMAGE, file.getName(), null, file.getContentType());
		} catch (IOException e) {
			throw new AppSystemException("Failed to save the uploaded input stream. File name: " + file.getName(), e);
		}
	}
	
	@Override
	public List<String> getRecentImageList(/* TODO: include pagination params */) {
		File blobsDir = new File(mediaStorePath + "/blobs");
		if (!blobsDir.exists()) {
			return Collections.emptyList();
		}
		try {
			return Files.list(blobsDir.toPath())
//				.filter(s -> s.toString().endsWith(".xml"))
					.sorted()
					.map(path -> path.toFile().getName())
					.collect(Collectors.toList());
		} catch (IOException e) {
			throw new AppSystemException("Failed to read the list of media files.", e);
		}
	}

}
