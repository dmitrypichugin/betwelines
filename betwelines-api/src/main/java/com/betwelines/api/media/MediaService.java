package com.betwelines.api.media;

import java.io.InputStream;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public interface MediaService {

	MediaDescriptor saveMediaDescriptor(MediaDescriptor mediaDescriptor);
	
	List<MediaDescriptor> getArticleMediaDescriptors(String version, String articleId);
	List<MediaDescriptor> getCardMediaDescriptors(String version, String cardId);

	void deleteMediaDescriptor(String id);

	/**
	 * This method saves a binary object in a store such file system or Amazon S3
	 * and returns the ID of the new object. The ID will be used in a subsequent
	 * call to saveMediaDecriptor().
	 */
	String saveBlob(InputStream inputStream);

	/**
	 * @see #saveBlob(InputStream)
	 * */
	String saveBlob(byte[] bytes);
	
	/**
	 * This method is an alternative to saveMediaDescriptor() and saveBlob() methods.
	 * It does two things in one shot: saves the blob in the store and creates a MediaDescriptor
	 * under the specified version/article.
	 * For now the plan is to have a global media file storage, like S3, and then articles
	 * would refer to them. And on the UI the user can lookup existing media files when adding
	 * a new file to an article. Let's say, the user wants to re-use a recently uploaded image
	 * in multiple articles.
	 * */
	MediaDescriptor saveMediaFile(String version, String articleId, MultipartFile file);
	
	MediaBlob saveMediaFile(MultipartFile file);
	List<String> getRecentImageList();
	
	InputStream getBlobStream(String blobId);

	void deleteBlob(String id);

}
