package com.betwelines.api.media;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.betwelines.api.exception.AppLogicException;
import com.betwelines.api.exception.AppSystemException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

@Service
public class MediaFsStoreImpl implements MediaStore {

	private static final String MEDIA = "betwelines-media";

	@Value("${media.store.path}")
	private String mediaStorePath;

	private String getParentFolder(String version, String parentId) {
		return mediaStorePath + "/" + MEDIA + "/" + version + "/" + parentId;
	}

	private File prepareDir(String version, String parentId, String fileName) {
		File path = new File(getParentFolder(version, parentId));
		if (!path.exists()) {
			try {
				Files.createDirectories(path.toPath());
			} catch (IOException e) {
				throw new AppSystemException("Failed to create directory " + path.toString(), e);
			}
		} else if (!path.isDirectory()) {
			throw new AppLogicException(
					"Failed to save the file " + fileName + ", the parent path is not a directory: " + path.toString());
		}
		return path;
	}

	@Override
	public void save(String version, String parentId, String fileName, byte[] bytes) {
		File path = prepareDir(version, parentId, fileName);
		try {
			Files.write(new File(path, fileName).toPath(), bytes);
		} catch (IOException e) {
			throw new AppSystemException("Failed to write the file " + path.toString() + " to disk", e);
		}
	}

	@Override
	public void save(String version, String parentId, String fileName, InputStream is) {
		File path = prepareDir(version, parentId, fileName);
		try {
			Files.copy(is, new File(path, fileName).toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new AppSystemException("Failed to write the file " + path.toString() + " to disk", e);
		}
	}

	@Override
	public void deleteFile(String version, String folderId, String fileName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteFolder(String version, String folderId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteVersion(String version) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(new File(mediaStorePath + "/" + MEDIA));
	}

//	@Override
//	public List<MediaDescriptor> getListOfFiles(String version, String parentId) {
//		String parentDir = getParentFolder(version, parentId);
//		File parentDirFile = new File(parentDir);
//		if (!parentDirFile.isDirectory()) {
//			return Collections.emptyList();
//		}
//		try (Stream<Path> pathListStream = Files.list(parentDirFile.toPath())) {
//			return pathListStream.map(path -> new MediaDescriptor(path.toFile().getName())).collect(Collectors.toList());
//		} catch (IOException e) {
//			throw new AppSystemException("Failed to read directory " + parentDir, e);
//		}
//	}
	
}