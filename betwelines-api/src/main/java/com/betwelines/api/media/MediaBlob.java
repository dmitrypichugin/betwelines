package com.betwelines.api.media;

public class MediaBlob {

	private String blobId;
	private MediaTypeEnum type;
	private String fileName;
	private String description;
	private String format;


	public MediaBlob() {}

	public MediaBlob(String blobId, MediaTypeEnum type, String fileName, String description, String format) {
		this.blobId = blobId;
		this.type = type;
		this.fileName = fileName;
		this.description = description;
		this.format = format;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public MediaTypeEnum getType() {
		return type;
	}

	public void setType(MediaTypeEnum type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getBlobId() {
		return blobId;
	}

	public void setBlobId(String blobId) {
		this.blobId = blobId;
	}

	@Override
	public String toString() {
		return "MediaBlob [blobId=" + blobId + ", type=" + type + ", fileName=" + fileName + ", description="
				+ description + ", format=" + format + "]";
	}
	
}
