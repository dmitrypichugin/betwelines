package com.betwelines.api.media;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
public class MediaController {

	@Autowired
	private MediaService mediaService;

	@Deprecated
	@GetMapping(value="/api/betwelines/{version}/media-folders/{folderId}")
	public List<MediaDescriptor> getMethodName(
			@PathVariable("version") String version,
			@PathVariable("folderId") String folderId) {
		
		return Collections.emptyList(); // mediaService.getListOfFiles(version, folderId);
	}

	/** @deprecated Use the getRecentImageList()/getRecentVideoList() methods instead. */
	@GetMapping("/api/betwelines/{version}/articles/{articleId}/media-descriptors")
	public List<MediaDescriptor> getMediaDescriptors(
			@PathVariable("version") String version,
			@PathVariable("articleId") String articleId) {
		
		return mediaService.getArticleMediaDescriptors(version, articleId);
	}
	
//	// First, we upload a file from the UI and return the blobId:
//	@PostMapping("/api/betwelines/media")
//	public String uploadFile(
//			@RequestParam("file") MultipartFile file) throws IOException {
//		
//		return mediaService.saveBlob(file.getInputStream());
//	}
//	
//	// Second, we submit the file descriptor with the blobId in it to store the meta data:
//	@PostMapping("/api/betwelines/{version}/articles/{articleId}/media-descriptors")
//	public MediaDescriptor saveMediaDescriptor(
//			@PathVariable("version") String version,
//			@PathVariable("articleId") String articleId,
//			MediaDescriptor mediaDescriptor) {
//		
//		return mediaService.saveMediaDescriptor(mediaDescriptor);
//	}

	// Second, we submit the file descriptor with the blobId in it to store the meta data:
	/** @deprecated Use the uploadMedia(MultipartFile file) method instead. */
	@PostMapping("/api/betwelines/{version}/articles/{articleId}/media")
	public MediaDescriptor uploadMedia(
			@PathVariable("version") String version,
			@PathVariable("articleId") String articleId,
			@RequestParam("file") MultipartFile file) {
		
		return mediaService.saveMediaFile(version, articleId, file);
	}
	
	@PostMapping("/api/betwelines/media/images")
	public MediaBlob uploadImage(@RequestParam("file") MultipartFile file) {
		return mediaService.saveMediaFile(file);
	}
	
	@GetMapping("/api/betwelines/media/images")
	public List<String> getRecentImageList() {
		return mediaService.getRecentImageList();
	}

	@DeleteMapping("/api/betwelines/media/images/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deleteImage(@PathVariable("id") String id) {
		mediaService.deleteBlob(id);
	}

}