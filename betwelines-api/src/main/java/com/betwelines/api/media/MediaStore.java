package com.betwelines.api.media;

import java.io.InputStream;

public interface MediaStore {

//	List<MediaDescriptor> getListOfFiles(String version, String parentId);

	void save(String version, String parentId, String fileName, byte[] bytes);
	void save(String version, String parentId, String fileName, InputStream is);

	void deleteFile(String version, String folderId, String fileName);

	void deleteFolder(String version, String folderId);

	void deleteVersion(String version);

	void deleteAll();
	
}