package com.betwelines.api.media;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface MediaDescriptorRepository extends MongoRepository<MediaDescriptor, String> {

	MediaDescriptor findByVersionAndArticleIdAndBlobId(String version, String articleId, String blobId);

	MediaDescriptor findByVersionAndCardIdAndBlobId(String version, String articleId, String blobId);

	List<MediaDescriptor> findByVersionAndArticleId(String version, String articleId);

	List<MediaDescriptor> findByVersionAndCardId(String version, String articleId);

}
