package com.betwelines.api.media;

import org.springframework.data.annotation.Id;

public class MediaDescriptor extends MediaBlob {

	@Id
	private String id;         // This is just a temporary ID in the DB, it's not the ID 
	                           // of the object in the blob store (S3, Azure, etc), that would be blobId
	
	private String version;
	private String articleId;
	private String cardId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	@Override
	public String toString() {
		return "MediaDescriptor [id=" + id + ", version=" + version + ", articleId=" + articleId + ", cardId=" + cardId
				+ ", " + super.toString() + "]";
	}

}
