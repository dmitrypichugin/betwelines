package com.betwelines.api.version;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import com.betwelines.api.article.ArticleVersionLink;
import com.betwelines.api.article.ArticleVersionLinkService;

@Service
public class VersionServiceImpl implements VersionService {

	@Autowired
	private VersionRepository versionRepo;
	@Autowired
	private ArticleVersionLinkService articleVersionLinkService;

	
	@Override
	public List<Version> getVersions() {
		return versionRepo.findAll();
	}

	@Override
	public Version getVersion(String id) {
		return versionRepo.findById(id)
				.orElseThrow(() -> new RuntimeException("Version [" + id + "] not found"));
	}

	@Override
	public Version saveVersion(Version version) {
		return versionRepo.findOne(Example.of(version, ExampleMatcher.matchingAll()))
				.orElseGet(() -> versionRepo.save(version));
	}

	@Override
	public void deleteVersion(String id) {
		versionRepo.deleteById(id);
	}

	@Override
	public void deleteAllVersions() {
		versionRepo.deleteAll();
	}

	@Override
	public Version createVersion(Version version) {
		List<Version> versions = getVersions();
		if (versions.size() > 0) {
			Version lastVersion = versions.get(versions.size() - 1);
			for (ArticleVersionLink link : articleVersionLinkService.findLinks(
					lastVersion.getLabel())) { //TODO: use ID
				
				articleVersionLinkService.saveArticleVersionLink(
						new ArticleVersionLink(
								link.getArticleId(),
								version.getLabel(), //TODO: use ID
								link.getHandle()));
			}
		}
		return versionRepo.save(version);
	}

}
