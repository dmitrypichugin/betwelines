package com.betwelines.api.version;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface VersionRepository extends MongoRepository<Version, String> {
}
