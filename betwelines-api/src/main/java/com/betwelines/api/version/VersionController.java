package com.betwelines.api.version;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.betwelines.api.security.SecurityConfig;

@Secured({SecurityConfig.ROLE_USER})
@RestController
public class VersionController {

	@Autowired
	private VersionService versionService;

	@RequestMapping(path = "/api/betwelines/versions", method = RequestMethod.GET)
	public List<Version> getVersions() {
		return versionService.getVersions();
	}

	@RequestMapping(path = "/api/betwelines/versions/{id}", method = RequestMethod.GET)
	public Version getVersion(@PathVariable("id") String id) {
		return versionService.getVersion(id);
	}

	@Secured({SecurityConfig.ROLE_ADMIN})
	@RequestMapping(path = "/api/betwelines/versions", method = RequestMethod.POST)
	public Version createVersion(@RequestBody Version version) {
		return versionService.createVersion(version);
	}

	@Deprecated
	@Secured({SecurityConfig.ROLE_ADMIN})
	@RequestMapping(path = "/api/betwelines/versions/{id}", method = RequestMethod.PUT)
	public Version updateVersion(@PathVariable("id") String id, @RequestBody Version version) {
		return versionService.saveVersion(version);
	}

	@Secured({SecurityConfig.ROLE_ADMIN})
	@RequestMapping(path = "/api/betwelines/versions/{id}", method = RequestMethod.DELETE)
	public void deleteVersion(@PathVariable("id") String id) {
		versionService.deleteVersion(id);
	}

}
