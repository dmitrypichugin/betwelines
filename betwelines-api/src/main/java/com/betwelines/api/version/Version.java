package com.betwelines.api.version;

import org.springframework.data.annotation.Id;

//@Entity
public class Version {

	@Id
	private String id;

	/**
	 * Indicated the order of the version object. The order never changes, so
	 * technically, the ID field could serve the purpose, but just so that we don't
	 * rely of the database implementation we have a separate field for this.
	 */
	private Short index;
	private String label;
	
	public Version() {}
	
	public Version(Short index, String label) {
		this.index = index;
		this.label = label;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Short getIndex() {
		return index;
	}

	public void setIndex(Short index) {
		this.index = index;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
