package com.betwelines.api.version;

import java.util.List;

public interface VersionService {

	List<Version> getVersions();

	Version getVersion(String id);

	/**
	 * This method initiates the full creation of a new version. 
	 * It create a Git branch and save the Version info in DB.
	 * 
	 * @param version
	 * @return Returns a Version object with the complete info about the newly created version.
	 */
	Version createVersion(Version version);

	/**
	 * Only saves the Version info in DB. This method does not alter the Git repository.
	 * 
	 * @param version
	 * @return
	 */
	Version saveVersion(Version version);

	void deleteVersion(String id);

	void deleteAllVersions();

}
