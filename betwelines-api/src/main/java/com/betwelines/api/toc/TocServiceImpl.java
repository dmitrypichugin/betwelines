package com.betwelines.api.toc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.betwelines.api.article.ArticleNode;
import com.betwelines.api.article.ArticleRepository;
import com.betwelines.api.article.ArticleVersionLinkRepository;

//@Profile("nogit")
@Service
public class TocServiceImpl implements TreeService {

	@Autowired
	private ArticleVersionLinkRepository articleVersionLinkRepo;
	@Autowired
	private ArticleRepository articleRepository;
	

	@Override
	public TocTree getTree(String version) {

		List<String> articleIds = articleVersionLinkRepo.findByVersion(version)
				.stream().map(link -> link.getArticleId()).collect(Collectors.toList());
		
		List<ArticleNode> nodes = articleRepository.findById(articleIds);
		
		TocTree toc = new TocTree();
		toc.setNodes(buildNodeTree(nodes));
		
		return toc;
	}

	//TODO: sort nodes
	private List<ArticleNode> buildNodeTree(List<ArticleNode> sourceNodes) {
		List<ArticleNode> rootNodes = new ArrayList<>();
		Map<String, ArticleNode> map = new HashMap<>(sourceNodes.size());
		for (ArticleNode srcNode : sourceNodes) {
			map.put(srcNode.getHandle(), srcNode);
		}
		for (ArticleNode srcNode : sourceNodes) {
			if (StringUtils.hasText(srcNode.getParentHandle())) {
				ArticleNode parent = map.get(srcNode.getParentHandle());
				if (parent != null) {
					List<ArticleNode> siblings = parent.getNodes();
					if (siblings == null) {
						siblings = new ArrayList<>();
						parent.setNodes(siblings);
					}
					siblings.add(srcNode);
				} else { // If the parent node somehow disappeared, we add this node to the root:
					rootNodes.add(srcNode);
				}
			} else {
				rootNodes.add(srcNode);
			}
		}
		return rootNodes;
	}

}
