package com.betwelines.api.toc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.betwelines.api.security.SecurityConfig;

@RestController
@Secured({SecurityConfig.ROLE_USER})
public class TreeController {
	
	@Autowired
	private TreeService treeService;

	@GetMapping("/api/betwelines/{version}/toc")
	public TocTree getTree(@PathVariable("version") String version) {
		return treeService.getTree(version);
	}

}
