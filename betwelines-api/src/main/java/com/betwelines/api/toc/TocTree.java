package com.betwelines.api.toc;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.betwelines.api.article.ArticleNode;

public class TocTree {

	@Id
	private String id;
	
	private List<ArticleNode> nodes;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<ArticleNode> getNodes() {
		return nodes;
	}

	public void setNodes(List<ArticleNode> nodes) {
		this.nodes = nodes;
	}

}
