package com.betwelines.api.toc;

public interface TreeService {

	TocTree getTree(String version);

}
