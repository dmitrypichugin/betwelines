package com.betwelines.api.index;

import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.betwelines.api.exception.AppSystemException;

@Service
public class IndexWriterServiceImpl implements IndexWriterService {
	
	@Autowired
	private Directory memoryIndex;
//	@Autowired
//	private IndexWriterConfig indexWriterConfig;
	
	private IndexWriterConfig getIndexWriterConfig() {
		StandardAnalyzer analyzer = new StandardAnalyzer();
		return new IndexWriterConfig(analyzer);
	}
	
	@Override
	public void addDocument(Document document) {
		try {
			IndexWriter writter = new IndexWriter(memoryIndex, getIndexWriterConfig());
			writter.addDocument(document);
			writter.close();
		} catch (IOException e) {
			throw new AppSystemException("Failed to create an index for the document.", e);
		}
	}

	@Override
	public void updateDocument(Term term, Document document) {
		try {
			IndexWriter writter = new IndexWriter(memoryIndex, getIndexWriterConfig());
			writter.updateDocument(term, document);
			writter.close();
		} catch (IOException e) {
			throw new AppSystemException("Failed to update the document index: " + term, e);
		}
	}

	@Override
	public void deleteDocument(Term term) {
		try {
			IndexWriter writter = new IndexWriter(memoryIndex, getIndexWriterConfig());
			writter.deleteDocuments(term);
			writter.close();
		} catch (IOException e) {
			throw new AppSystemException("Failed to delete the document index: " + term, e);
		}
	}

}
