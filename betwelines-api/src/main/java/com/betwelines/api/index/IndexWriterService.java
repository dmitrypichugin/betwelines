package com.betwelines.api.index;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;

public interface IndexWriterService {

	void addDocument(Document doc);

	void updateDocument(Term term, Document document);

	void deleteDocument(Term term);

}
