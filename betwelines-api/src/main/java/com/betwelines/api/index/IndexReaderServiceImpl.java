package com.betwelines.api.index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.betwelines.api.exception.AppSystemException;

@Service
public class IndexReaderServiceImpl implements IndexReaderService {

	@Autowired
	private Directory memoryIndex;

	@Override
	public List<Document> searchIndex(Query query, Integer pageSize, Integer from) {
		try {
			IndexReader indexReader = DirectoryReader.open(memoryIndex);
			IndexSearcher searcher = new IndexSearcher(indexReader);
	
			if (from == null) {
				from = 0;
			}
			// Lucene seems to have no better way of skipping records:
			TopDocs topDocs = searcher.search(query, from + pageSize);
			int to = Math.min(topDocs.scoreDocs.length - from, pageSize);
			List<Document> documents = new ArrayList<>();
//			for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			for (int i = from; i < to; i++) {
				ScoreDoc scoreDoc = topDocs.scoreDocs[i];
				documents.add(searcher.doc(scoreDoc.doc));
			}
			indexReader.close();
			return documents;
		} catch (IOException e) {
			throw new AppSystemException("Failed to search index: " + query, e);
		}
	}
	
}
