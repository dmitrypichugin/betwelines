package com.betwelines.api.index;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;

public interface IndexReaderService {

	List<Document> searchIndex(Query query, Integer pageSize, Integer from);

}
