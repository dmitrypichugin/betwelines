Introduction
========

Betwelines is a documentation management system. 

Features
-------
- Multiple version support in real-time
- Table of Content
- Embedded 'Card' support
- Efficient storage consumption
- Self-hosted installation
- Cloud-based hosting
- Support for both private and public repositories
- Design customization

Most suitable applications
----------------------
- Product documentation
- Simple informational website

Technology stack
---------------
- Java
- MongoDB
- Lucene
- Angular
- Markdown document format
- Git-based versioning engine


