Directory structure
============

data/
	header.json
	articles/
		123/                                                                                                        // consider using handle instead of ID as the folder name
			meta.art.json
															{
																articleId: 123,
																tocLabel: "My Mega Top Article",      //what appears in the TOC menu and in the breadcrumbs
																handle: "my-mega-top-article",       //what appears in the URL:   /betwelines/V.1.2.3/my-mega-top-article/my-sub-article
																order: 9,                                          // might be used in the future to sort articles within the parent
																created: "2020-02-21T10:14:00.000Z", // ISO format (new Date().toISOString() in JavaScript)
																updated: "2020-02-21T10:14:00.000Z" 
															}
			content.art.md
			media/
				<UUID>.json
															{                                                          // MediaDescriptor.java
																"blobId": 123,                                  // UUID - the same as in the filename
																"type": "IMAGE",                               // Enumeration (IMAGE, VIDEO, etc)
																"fileName": "my-image.jpg",
																"description": "",
																"format": "jpg"
															}
			cards/
				123/
					media/
					meta.card.json
															{
																"cardId": 1,
																"handle": "card-one",
																"created": "2020-02-21T10:14:00.000Z",
																"updated": "2020-02-21T10:14:00.000Z"
															}
					content.card.md
			articles/                                                                                             // this is the only recursive thing
				...
			